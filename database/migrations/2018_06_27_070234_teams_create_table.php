<?php

use PickSuite\App\Models\Sport;
use PickSuite\App\Models\Team;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class TeamsCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Team::TABLE, function (Blueprint $table) {
            $table->init();
            $table->string(Team::ATTR_COVERS_ID);
            $table->string(Team::ATTR_ABBR);
            $table->relate(Team::ATTR_SPORT_ID, Sport::class);

            $table->unique([Team::ATTR_COVERS_ID]);
            $table->unique([Team::ATTR_SPORT_ID, Team::ATTR_ABBR]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Team::TABLE);
    }
}
