<?php

use PickSuite\App\Models\Game;
use PickSuite\App\Models\Sport;
use PickSuite\App\Models\Team;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class GamesCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Game::TABLE, function (Blueprint $table) {
            $table->init();
            $table->string(Game::ATTR_COVERS_ID);
            $table->string(Game::ATTR_STATUS)->default('');
            $table->dateTime(Game::ATTR_STARTS_AT);
            $table->float(Game::ATTR_AWAY_MONEY_LINE)->nullable();
            $table->float(Game::ATTR_HOME_MONEY_LINE)->nullable();
            $table->float(Game::ATTR_OVER_UNDER)->nullable();
            $table->relate(Game::ATTR_SPORT_ID, Sport::class);
            $table->relate(Game::ATTR_AWAY_TEAM_ID, Team::class);
            $table->relate(Game::ATTR_HOME_TEAM_ID, Team::class);

            $table->unique(Game::ATTR_COVERS_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Game::TABLE);
    }
}
