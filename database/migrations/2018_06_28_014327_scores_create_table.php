<?php

use PickSuite\App\Models\Game;
use PickSuite\App\Models\Score;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ScoresCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Score::TABLE, function (Blueprint $table) {
            $table->init();
            $table->string(Score::ATTR_INDEX);
            $table->integer(Score::ATTR_AWAY_SCORE);
            $table->integer(Score::ATTR_HOME_SCORE);
            $table->relate(Score::ATTR_GAME_ID, Game::class);

            $table->unique([Score::ATTR_GAME_ID, Score::ATTR_INDEX]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Score::TABLE);
    }
}
