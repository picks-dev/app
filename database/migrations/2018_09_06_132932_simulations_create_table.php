<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use PickSuite\App\Models\Simulation;
use PickSuite\App\Models\Sport;
use PickSuite\App\Models\Team;

class SimulationsCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Simulation::TABLE, function (Blueprint $table) {
            $table->init();
            $table->text(Simulation::ATTR_SETTINGS);
            $table->text(Simulation::ATTR_RESULTS);
            $table->float(Simulation::ATTR_CONFIDENCE);
            $table->float(Simulation::ATTR_ACCURACY)->nullable();
            $table->boolean(Simulation::ATTR_SIDES);
            $table->date(Simulation::ATTR_TARGET_DATE);
            $table->relate(Simulation::ATTR_TEAM_ID, Team::class);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Simulation::TABLE);
    }
}
