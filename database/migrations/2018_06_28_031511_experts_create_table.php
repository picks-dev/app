<?php

use PickSuite\App\Models\Expert;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ExpertsCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Expert::TABLE, function (Blueprint $table) {
            $table->init();
            $table->string(Expert::ATTR_COVERS_ID);

            $table->unique(Expert::ATTR_COVERS_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Expert::TABLE);
    }
}
