<?php

use PickSuite\App\Models\Sport;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SportsCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Sport::TABLE, function (Blueprint $table) {
            $table->init();
            $table->string(Sport::ATTR_NAME);
            $table->string(Sport::ATTR_ABBR);
            $table->boolean(Sport::ATTR_IS_MONEY_LINE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Sport::TABLE);
    }
}
