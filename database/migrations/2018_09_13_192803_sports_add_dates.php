<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use PickSuite\App\Models\Sport;

class SportsAddDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Sport::TABLE, function (Blueprint $table) {
            $table->date(Sport::ATTR_FROM)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Sport::TABLE, function (Blueprint $table) {
            $table->removeColumn(Sport::ATTR_FROM);
        });
    }
}
