<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use PickSuite\App\Models\Expert;
use PickSuite\App\Models\Game;
use PickSuite\App\Models\Pick;
use PickSuite\App\Models\Team;

class PicksCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Pick::TABLE, function (Blueprint $table) {
            $table->init();
            $table->unsignedInteger(Pick::ATTR_RANK);
            $table->boolean(Pick::ATTR_TOTAL_OVER)->nullable();
            $table->string(Pick::ATTR_TOTAL_POINT)->nullable();
            $table->relate(Pick::ATTR_GAME_ID, Game::class);
            $table->relate(Pick::ATTR_EXPERT_ID, Expert::class);
            $table->relate(Pick::ATTR_TEAM_ID, Team::class);
            $table->relate(Pick::ATTR_SIDE_ID, Team::class)[0]->nullable();
            $table->unique([
                Pick::ATTR_GAME_ID,
                Pick::ATTR_EXPERT_ID,
                Pick::ATTR_TOTAL_OVER,
                Pick::ATTR_TOTAL_POINT,
                Pick::ATTR_SIDE_ID,
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Pick::TABLE);
    }
}
