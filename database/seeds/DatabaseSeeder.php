<?php

use Illuminate\Database\Seeder;
use PickSuite\App\Helpers\Arr;
use PickSuite\App\Models\Sport;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->seedSports();
    }

    private function seedSports()
    {
        $existing = Sport::query()->get()->keyBy(Sport::ATTR_UUID);
        Arr::map(Sport::listInMemory(), function (Sport $sport, string $uuid) use ($existing) {
            ($existing[$uuid] ?? $sport)
                ->fill($sport->getAttributes())
                ->save();
        });
    }
}
