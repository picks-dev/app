<?php /** @var PickSuite\App\Views\Forms\SuggestForm $vm */ ?>
<form id="<?= $vm->id ?>" class="mb-3">
    <div class="form-row">
        <div class="form-group col">
            <label for="sport">Sport</label>
            <select id="sport" name="sport" class="form-control">
                <?php foreach($vm->sportOptions as $uuid => $sport): ?>
                <option value="<?= $uuid ?>" <?= $vm->sportIsSelected($uuid) ?>><?= $sport->abbr ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col">
            <label for="team">Team</label>
            <select id="team" name="teams[]" class="form-control custom-select-lg" multiple size="1">
                <?php foreach($vm->teamOptions as $uuid => $team): ?>
                <option value="<?= $uuid ?>" <?= $vm->teamIsSelected($uuid) ?>><?= $team->abbr ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-10">
            <label for="to">To</label>
            <input type="date" class="form-control" id="to" name="to" value="<?= $vm->to ?>">
        </div>
        <div class="col-md-1">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="sides" name="sides" <?= $vm->sidesIsChecked() ?>>
                <label class="form-check-label" for="sides">Sides</label>
            </div>
        </div>
        <div class="col-md-1">
            <button type="submit" class="btn btn-primary">Apply</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $("#<?= $vm->id ?> select").chosen();
</script>
