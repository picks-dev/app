<?php /** @var PickSuite\App\Views\Forms\SpreadsheetForm $vm */ ?>
<form id="<?= $vm->id ?>" class="mb-3">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="sport">Sport</label>
            <select id="sport" name="sport" class="form-control">
                <?php foreach($vm->sportOptions as $uuid => $sport): ?>
                <option value="<?= $uuid ?>" <?= $vm->sportIsSelected($uuid) ?>><?= $sport ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="from">From</label>
            <input type="date" class="form-control" id="from" name="from" value="<?= $vm->from ?>">
        </div>
        <div class="form-group col-md-3">
            <label for="to">To</label>
            <input type="date" class="form-control" id="to" name="to" value="<?= $vm->to ?>">
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Apply</button>
    <button type="submit" class="btn btn-warning" name="scrape" value="1">Scrape</button>
</form>
<script type="text/javascript">
    $("#<?= $vm->id ?> select").chosen();
</script>
