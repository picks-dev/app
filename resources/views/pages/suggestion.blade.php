<?php /** @var \PickSuite\App\Views\Pages\SuggestionPage $vm */ ?>
@extends('layouts.default')
@section('body')
    <div class="m-4">
        {!! $vm->getForm() !!}
        <?php foreach ($vm->getTables() as $label => $table): ?>
        <nav class="navbar navbar-dark bg-secondary mb-2">
            <span class="navbar-brand"><?= $label ?></span>
        </nav>
        {!! $table !!}
        <?php endforeach; ?>
    </div>
@endsection
