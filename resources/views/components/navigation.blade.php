<?php /** @var PickSuite\App\Views\Components\Navigation $vm */ ?>
<ul class="nav <?= $vm->getOrientation() ?>">
    <?php foreach($vm->getLinks() as $label => [$href, $attributes]): ?>
    <li class="nav-item">
        <a href="<?= $href ?>"
           class="nav-link btn btn-outline-primary btn-sm m-2" <?= $attributes ?>><?= $label ?></a>
    </li>
    <?php endforeach; ?>
</ul>
