<?php /** @var PickSuite\App\Views\Components\ScrapeTimesHeader $vm */ ?>
<nav class="navbar navbar-dark bg-secondary mb-2 rounded">
    <a class="navbar-brand" href="<?= $vm->getHref() ?>" target="_blank"><?= $vm->getBrand() ?></a>
    <div class="row">
        <?php foreach ($vm->getScrapeTimes() as $text): ?>
        <span class="mx-2 my-1 badge badge-info badge-pill badge-dark bg-dark"><?= $text ?></span>
        <?php endforeach; ?>
    </div>
</nav>
