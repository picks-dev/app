<?php /** @var PickSuite\App\Views\Components\Table $vm */ ?>
<button class="badge badge-pill my-1" data-copy="<?= $vm->toCsv() ?>">Copy</button>
<table class="table table-sm">
    <thead>
    <tr>
        <?php foreach ($vm->getHeaders() as $header): ?>
        <th><?= $header ?></th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach($vm->getRows() as $row): ?>
    <tr>
        <?php foreach($row as $i => $column): ?>
        <td><?= $column ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
