<?php /** @var PickSuite\App\Views\Components\SiteHeader $vm */ ?>
<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="<?= $vm->getHref() ?>"><?= $vm->getBrand() ?></a>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="<?= $vm->getSpreadsheetHref() ?>">Spreadsheet</a>
        </li>
    </ul>
</nav>
