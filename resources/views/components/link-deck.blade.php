<?php /** @var PickSuite\App\Views\Components\LinkDeck $vm */ ?>
<div class="card-columns">
    <?php foreach($vm->getLinks() as $label => [$href, $attributes]): ?>
        <a class="card badge badge-light" <?= $attributes ?> href="<?= $href ?>">
            <div class="card-body">
                <h5 class="card-title"><?= $label ?></h5>
            </div>
        </a>
    <?php endforeach; ?>
</div>
