<?php /** @var PickSuite\App\Views\ViewModel $vm */ ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
    @yield('head')
    <?php foreach($vm->getCss() as $css): ?>
    <link type="text/css" rel="stylesheet" href="{!! $css !!}">
    <?php endforeach; ?>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <?php foreach($vm->getJs() as $js): ?>
    <script type="text/javascript" src="{{ $js }}"></script>
    <?php endforeach; ?>
    @yield('styles')
</head>
<body>
<?= new \PickSuite\App\Views\Components\SiteHeader ?>
@yield('body')
@yield('scripts')
</body>
</html>
