# PickSuite/App

A project for composing the PickSuite and providing various IO functionality.

For convenience, the PickSuite/Data package has been entirely integrated, largely represented by the PickSuite/App/Models namespace.

---

# PickSuite/WebScraper

A lightweight library for scraping data from [covers.com](https://www.covers.com/).

# Vocabulary

* Page: the entirety of a web page's  HTML
* Partial: a subset of HTML inside a web page

# Library Structure

### [Covers](./src/Covers)

There are specific implementations of the [HtmlParser](./src/HtmlParser.php) interface written for the various pages and partials found on Covers.com. 

### [Requests](./src/Requests)

The purpose of these classes is to describe how to find a remote (Covers.com) web page (either over the web or in a cache).

They compose given data to provide:

* URL: where to find any given web page
* CacheTTL: (Time-to-Live) how long any given web page should be cached, to prevent [DoS](https://en.wikipedia.org/wiki/Denial-of-service_attack) / [Rate Limiting](https://en.wikipedia.org/wiki/Rate_limiting) by Covers.com or similar.

### [Impl](./src/Impl)

These are partial or complete implementations of the various scraper interfaces, provided for convenience.

* [DomParser](src/Impl/DomParser.php) is a very simple wrapper on top of the DOMDocument/DOMXpath/DOMNode PHP classes. It provides a certain amount of convenience in parsing html.
* [PickScraper](./src/Impl/PickScraper.php) composes an [HttpClient](./src/HttpClient.php) and [HtmlCache](./src/HtmlCache.php) in order to execute [ScrapeRequests](./src/ScrapeRequest.php), fetching HTML from the web (or a cache) so that it can be parsed by an [HtmlParser](src/HtmlParser.php).

# Usage

```php
<?php

use PickSuite\WebScraper\Covers\BoxScorePage;
use PickSuite\WebScraper\Covers\ConsensusPage;
use PickSuite\WebScraper\Covers\GamesPage;
use PickSuite\WebScraper\Covers\PickPartial;
use PickSuite\WebScraper\Covers\PicksPartial;
use PickSuite\WebScraper\Covers\TeamPage;
use PickSuite\WebScraper\Covers\TeamsPage;
use PickSuite\WebScraper\Impl\FileCache;
use PickSuite\WebScraper\Impl\FileTransport;
use PickSuite\WebScraper\Impl\PickScraper;
use PickSuite\WebScraper\Requests\ScrapeBoxScore;
use PickSuite\WebScraper\Requests\ScrapeConsensus;
use PickSuite\WebScraper\Requests\ScrapeGames;
use PickSuite\WebScraper\Requests\ScrapePicks;
use PickSuite\WebScraper\Requests\ScrapeTeam;
use PickSuite\WebScraper\Requests\ScrapeTeams;

/**
 * PREPARE SERVICES
 */
$httpClient = new FileTransport;
$htmlCache = new FileCache;
$pickScraper = new PickScraper($httpClient, $htmlCache);

/**
 * SCRAPE TEAMS
 */
$sport = 'mlb';
$teamsReq = new ScrapeTeams($sport);
$teamsHtml = $pickScraper->execute($teamsReq);
$teamsPage = new TeamsPage($teamsHtml);

/** @var TeamPage[] $teamPages */
$teamPages = [];
foreach ($teamsPage->getTeamsHrefs() as $href) {
    $teamReq = new ScrapeTeam($href);
    $teamHtml = $pickScraper->execute($teamReq);
    $teamPages[] = new TeamPage($teamHtml);
}

/**
$teamPages contents...

Array
(
    [0] => PickSuite\WebScraper\Covers\TeamPage Object
        (
            [id] => 2959
            [abbr] => bal
            [name] => Baltimore Orioles
        )

    [1] => PickSuite\WebScraper\Covers\TeamPage Object
        (
            [id] => 2966
            [abbr] => bos
            [name] => Boston Red Sox
        )

    etc...
)
*/

/**
 * SCRAPE GAMES
 */
$date = new DateTime('3 days ago');
$gamesReq = new ScrapeGames($sport, $date);
$gamesHtml = $pickScraper->execute($gamesReq);
$gamesPage = new GamesPage($gamesHtml);
$gameBoxes = $gamesPage->getGameBoxes();
/**
$gameBoxes contents...

Array
(
    [0] => PickSuite\WebScraper\Covers\GameBoxPartial Object
        (
            [id] => 566857
            [status] => final
            [start_time] => DateTime Object
                (
                    [date] => 2018-08-02 13:10:00.000000
                    [timezone_type] => 3
                    [timezone] => America/New_York
                )

            [away_team_id] => 2979
            [home_team_id] => 2960
            [over_under] => 7.5
            [away_money_line] => 125
            [home_money_line] => -135
            [away_scores] => Array
                (
                    [0] => 0
                    [1] => 0
                    [2] => 0
                    [3] => 0
                    [4] => 0
                    [5] => 0
                    [6] => 2
                    [7] => 0
                    [8] => 0
                )

            [home_scores] => Array
                (
                    [0] => 0
                    [1] => 0
                    [2] => 0
                    [3] => 4
                    [4] => 0
                    [5] => 0
                    [6] => 0
                    [7] => 0
                    [8] => 0
                )

            [consensus_href] => https://contests.covers.com/Consensus/MatchupConsensusDetails?externalId=%2fsport%2fbaseball%2fcompetition%3a566857
        )

    etc...
)
*/

/**
 * SCRAPE META & PICKS
 */

/** @var BoxScorePage[] $boxScorePages */
$boxScorePages = [];
/** @var PickPartial[] $pickPartials */
$pickPartials = [];
foreach ($gameBoxes as $gameBox) {
    // Scrape BoxScore
    $boxScoreReq = new ScrapeBoxScore($gameBox->boxscore_href);
    $boxScoreHtml = $pickScraper->execute($boxScoreReq);
    $boxScorePages[] = new BoxScorePage($boxScoreHtml);

    // Scrape Consensus
    $consensusReq = new ScrapeConsensus($gameBox->consensus_href);
    $consensusHtml = $pickScraper->execute($consensusReq);
    $consensusPage = new ConsensusPage($consensusHtml);

    $picksRequest = new ScrapePicks($consensusPage->getCompetitionId());
    $picksHtml = $pickScraper->execute($picksRequest);
    $picksPage = new PicksPartial($picksHtml);
    $pickPartials += $picksPage->getPicks($consensusPage->getAwayAbbr());
}

/**
$boxScorePages contents...

Array
(
    [0] => PickSuite\WebScraper\Covers\BoxScorePage Object
        (
            [awayPitcherId] => 43161
            [awayPitcherName] => Homer Bailey
            [homePitcherId] => 112687
            [homePitcherName] => Noah Syndergaard
            [umpireId] => 8369
            [umpireName] => Kerwin Danley
        )

    [1] => PickSuite\WebScraper\Covers\BoxScorePage Object
        (
            [awayPitcherId] => 105145
            [awayPitcherName] => Kyle Gibson
            [homePitcherId] => 113187
            [homePitcherName] => Trevor Bauer
            [umpireId] => 8402
            [umpireName] => Marvin Hudson
        )

    etc...
)

$pickPartials contents...

Array
(
    [0] => PickSuite\WebScraper\Covers\PickPartial Object
        (
            [total_point] => null
            [total_over] => null
            [expert_id] => 807b96bc-c614-49e6-911e-a887017698dd
            [rank] => 4
            [team_is_away] => false
            [side_is_away] => true
        )

    [1] => PickSuite\WebScraper\Covers\PickPartial Object
        (
            [total_point] => null
            [total_over] => null
            [expert_id] => c25cda28-d666-4bf2-bd50-a89001011168
            [rank] => 3
            [team_is_away] => true
            [side_is_away] => true
        )

    etc...
)
*/
```

---

# PickSuite/Profiler

A simple wrapper around the [Tideways](https://tideways.com/) extension