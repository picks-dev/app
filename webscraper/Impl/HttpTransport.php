<?php

namespace PickSuite\WebScraper\Impl;

use App;
use Cache;
use Event;
use GuzzleHttp\Client;
use PickSuite\WebScraper\Events\HttpRequest;
use PickSuite\WebScraper\HttpClient;

class HttpTransport implements HttpClient
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function get(string $url): string
    {
        if (App::environment() === 'local' && Cache::has($url)) {
            return Cache::get($url);
        }
        Event::dispatch(new HttpRequest($url));
        $result = $this->client->get($url)->getBody()->getContents();
        Cache::put($url, $result, 30);
        return $result;
    }
}
