<?php

namespace PickSuite\WebScraper\Impl;

use PickSuite\WebScraper\HttpClient;
use PickSuite\WebScraper\ScrapeRequest;
use PickSuite\WebScraper\WebScraper;

class PickScraper implements WebScraper
{
    private $client;

    public function __construct(HttpClient $client)
    {
        $this->client = $client;
    }

    public function execute(ScrapeRequest $request): string
    {
        $url = $request->getUrl();
        $html = $this->client->get($url);
        return $html;
    }
}
