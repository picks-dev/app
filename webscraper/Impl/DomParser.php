<?php

namespace PickSuite\WebScraper\Impl;

use DOMDocument;
use DOMNode;
use DOMXPath;
use PickSuite\WebScraper\HtmlParser;

class DomParser implements HtmlParser
{
    public static $SELECTORS = [];
    /** @var DOMNode */
    private $node;

    /**
     * @param DOMNode|HtmlParser|string $html
     */
    public function __construct($html)
    {
        if ($html instanceof DOMNode) {
            $this->node = $html;
        } elseif ($html instanceof DomParser) {
            $this->node = $html->getNode();
        } elseif (is_string($html)) {
            $dom = new DOMDocument;
            $internalErrors = libxml_use_internal_errors(true);
            $dom->loadHTML($html);
            libxml_use_internal_errors($internalErrors);
            $this->node = $dom->documentElement;
        } else {
            throw new \InvalidArgumentException("HTML must be DOMNode or string");
        }
    }

    public function getNode(): DOMNode
    {
        return $this->node;
    }

    public function attr(string $name, string $default = ''): string
    {
        return $this->node->attributes->getNamedItem($name)->nodeValue ?? $default;
    }

    public function select(string $path): ?HtmlParser
    {
        return $this->filter($path)[0] ?? null;
    }

    /**
     * @param string $path
     * @return HtmlParser[]
     */
    public function filter(string $path): array
    {
        $xpath = "{$this->node->getNodePath()}/descendant-or-self::{$path}";
        $nodes = (new DOMXPath($this->node->ownerDocument))->query($xpath);
        return array_map(function (DOMNode $node) {
            return new DomParser($node);
        }, array_filter(iterator_to_array($nodes), function ($node) {
            return $node instanceof DOMNode;
        }));
    }

    public function text(): string
    {
        return $this->node->textContent;
    }

    public function __toString()
    {
        return $this->node->ownerDocument->saveHTML($this->node);
    }
}
