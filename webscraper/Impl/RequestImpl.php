<?php

namespace PickSuite\WebScraper\Impl;

trait RequestImpl
{
    private $url;

    public function getUrl(): string
    {
        return $this->url;
    }
}
