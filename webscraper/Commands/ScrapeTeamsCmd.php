<?php

namespace PickSuite\WebScraper\Commands;

use Bus;
use Illuminate\Console\Command;
use PickSuite\App\Jobs\ScrapeTeams;

class ScrapeTeamsCmd extends Command
{
    protected $signature = 'scrape:teams
    {sport : The sport (abbreviation) you want to scrape}';

    protected $description = 'Scrape covers.com for teams';

    public function handle()
    {
        $sport = $this->argument('sport');
        $job = new ScrapeTeams($sport);
        Bus::dispatch($job);
    }
}
