<?php

namespace PickSuite\WebScraper\Commands;

use Bus;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use PickSuite\App\Jobs\ScrapePicks;

class ScrapePicksCmd extends Command
{
    protected $signature = 'scrape:picks
    {sport : The sport (abbreviation) you want to scrape}
    {date : Date to scrape}
    {from : Date from which to begin (default: date)}
    {--recurring : Whether this scrape should repeat with a splay}';

    protected $description = 'Scrape covers.com for picks';

    public function handle()
    {
        $sport = $this->argument('sport');
        $to = Carbon::parse($this->argument('date'))->endOfDay();
        $from = Carbon::parse($this->argument('from') ?: $to)->endOfDay();
        $recurring = $this->option('recurring');

        while ($from->lt($to)) {
            $job = new ScrapePicks($sport, $to, $recurring && $from->eq($to));
            Bus::dispatch($job);
            $from->addDay();
        }
    }
}
