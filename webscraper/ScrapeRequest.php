<?php

namespace PickSuite\WebScraper;

interface ScrapeRequest
{
    public function getUrl(): string;
}
