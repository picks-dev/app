<?php

namespace PickSuite\WebScraper\Requests;

use PickSuite\WebScraper\Impl\RequestImpl;
use PickSuite\WebScraper\ScrapeRequest;

class ScrapePicks implements ScrapeRequest
{
    use RequestImpl;

    public function __construct(string $competitionId)
    {
        $this->url = "https://contests.covers.com/Consensus/MatchupConsensusExpertDetails/{$competitionId}";
    }
}
