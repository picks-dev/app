<?php

namespace PickSuite\WebScraper\Requests;

use PickSuite\WebScraper\Impl\RequestImpl;
use PickSuite\WebScraper\ScrapeRequest;

class ScrapeTeams implements ScrapeRequest
{
    use RequestImpl;

    public function __construct(string $sport)
    {
        $sport = strtolower($sport);
        $this->url = "https://www.covers.com/pageLoader/pageLoader.aspx?page=/data/{$sport}/teams/teams.html";
    }
}
