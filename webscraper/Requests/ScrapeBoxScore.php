<?php

namespace PickSuite\WebScraper\Requests;

use PickSuite\WebScraper\Impl\RequestImpl;
use PickSuite\WebScraper\ScrapeRequest;

class ScrapeBoxScore implements ScrapeRequest
{
    use RequestImpl;

    public function __construct(string $boxScoreHref)
    {
        $this->url = $boxScoreHref;
    }
}
