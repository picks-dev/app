<?php

namespace PickSuite\WebScraper\Requests;

use PickSuite\WebScraper\Impl\RequestImpl;
use PickSuite\WebScraper\ScrapeRequest;

class ScrapeConsensus implements ScrapeRequest
{
    use RequestImpl;

    public function __construct(string $href)
    {
        $this->url = $href;
    }
}
