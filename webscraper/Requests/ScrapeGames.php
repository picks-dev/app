<?php

namespace PickSuite\WebScraper\Requests;

use DateTime;
use PickSuite\WebScraper\Impl\RequestImpl;
use PickSuite\WebScraper\ScrapeRequest;

class ScrapeGames implements ScrapeRequest
{
    use RequestImpl;

    public function __construct(string $sport, DateTime $date)
    {
        $sport = strtolower($sport);
        $this->url = "https://www.covers.com/Sports/{$sport}/Matchups?selectedDate={$date->format('Y-m-d')}";
    }
}
