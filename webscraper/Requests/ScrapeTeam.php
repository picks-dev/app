<?php

namespace PickSuite\WebScraper\Requests;

use PickSuite\WebScraper\Impl\RequestImpl;
use PickSuite\WebScraper\ScrapeRequest;

class ScrapeTeam implements ScrapeRequest
{
    use RequestImpl;

    public function __construct(string $href)
    {
        $this->url = "https://www.covers.com{$href}";
    }
}
