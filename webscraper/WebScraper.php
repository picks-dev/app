<?php

namespace PickSuite\WebScraper;

interface WebScraper
{
    public function execute(ScrapeRequest $request): string;
}
