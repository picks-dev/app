<?php

namespace PickSuite\WebScraper\Covers;

use PickSuite\WebScraper\Impl\DomParser;

class TeamPage extends DomParser
{
    public $id;
    public $abbr;
    public $name;

    public function __construct($html)
    {
        parent::__construct($html);
        $this->id = $this->getId();
        $this->abbr = $this->getAbbr();
        $this->name = $this->getName();
    }

    private function getId(): string
    {
        if ($link = $this->select('meta[@id="ogUrl"]')) {
            preg_match('/\/team([0-9]+)\.html$/', $link->attr('content'), $matches);
            return end($matches);
        }

        return '';
    }

    private function getAbbr(): string
    {
        if ($teamLogo = $this->select('div[contains(@class,"teamlogo")]/img')) {
            preg_match('/\/([a-z]+)\.gif$/', $teamLogo->attr('src'), $matches);
            return end($matches);
        }

        return '';
    }

    private function getName(): string
    {
        if ($teamName = $this->select('div[contains(@class,"teamname")]/h1[contains(@class,"teams")]')) {
            return trim(preg_replace('/\s+/', ' ', $teamName->text()));
        }

        return '';
    }

}
