<?php

namespace PickSuite\WebScraper\Covers;

use PickSuite\WebScraper\Impl\DomParser;
use function preg_match;
use function preg_replace;

class BoxScorePage extends DomParser
{
    public $awayPitcherId;
    public $awayPitcherName;
    public $homePitcherId;
    public $homePitcherName;
    public $umpireId;
    public $umpireName;

    public function __construct($html)
    {
        parent::__construct($html);
        [$this->awayPitcherId, $this->awayPitcherName] = $this->getAwayPitcher();
        [$this->homePitcherId, $this->homePitcherName] = $this->getHomePitcher();
        [$this->umpireId, $this->umpireName] = $this->getUmpire();
    }

    private function getAwayPitcher(): array
    {
        if ($awayPitcherLink = $this->select('*[contains(@class,"num")][2]/tr[3]/td/a')) {
            preg_match('/player([0-9]+)\.html/', $awayPitcherLink->attr('href'), $matches);
            $id = end($matches);
            $name = trim(preg_replace('/\s+/', ' ', $awayPitcherLink->text()));
            return [$id, $name];
        }
        if ($homePitcherTd = $this->select('*[contains(@class,"num")][4]/tr[3]/td')) {
            $name = trim(preg_replace('/\s+/', ' ', $homePitcherTd->text()));
            return [null, $name];
        }

        return [null, null];
    }

    private function getHomePitcher(): array
    {
        if ($homePitcherLink = $this->select('*[contains(@class,"num")][4]/tr[3]/td/a')) {
            preg_match('/player([0-9]+)\.html/', $homePitcherLink->attr('href'), $matches);
            $id = end($matches);
            $name = trim(preg_replace('/\s+/', ' ', $homePitcherLink->text()));
            return [$id, $name];
        }
        if ($homePitcherTd = $this->select('*[contains(@class,"num")][4]/tr[3]/td')) {
            $name = trim(preg_replace('/\s+/', ' ', $homePitcherTd->text()));
            return [null, $name];
        }
        return [null, null];
    }

    private function getUmpire(): array
    {
        if ($umpireLink = $this->select('*[contains(@class,"row")][last() - 4]/a[2]')) {
            preg_match('/umpire([0-9]+)\.html/', $umpireLink->attr('href'), $matches);
            $id = end($matches);
            $name = trim(preg_replace('/\s+/', ' ', $umpireLink->text()));
            return [$id, $name];
        }

        return [null, null];
    }
}
