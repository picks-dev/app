<?php

namespace PickSuite\WebScraper\Covers;

use DateTime;
use DateTimeZone;
use function floatval;
use PickSuite\WebScraper\HtmlParser;
use PickSuite\WebScraper\Impl\DomParser;
use function preg_replace;
use function stristr;
use function strtolower;

class GameBoxPartial extends DomParser
{
    public $id;
    public $status;
    public $start_time;
    public $away_team_id;
    public $home_team_id;
    public $over_under;
    public $away_money_line;
    public $home_money_line;
    public $away_scores;
    public $home_scores;
    public $consensus_href;
    public $boxscore_href;
    public $isLeagueGame;

    public function __construct($html)
    {
        parent::__construct($html);
        [$awayTeamId, $homeTeamId] = $this->getTeamIds();
        $overUnder = null;
        $awayMoneyLine = null;
        $homeMoneyLine = null;
        $awayScores = [];
        $homeScores = [];

        foreach ($this->getTableData() as $header => [$top, $bot]) {
            if (is_numeric($header) || $header === 'OT') {
                $awayScores[$header] = (int)$top;
                $homeScores[$header] = (int)$bot;
            }
            if ($header === 'ML') {
                $awayMoneyLine = (float)$top;
                $homeMoneyLine = (float)$bot;
            }
            if ($header === 'O/U') {
                $overUnder = (float)($top ?: $bot);
            }
        }

        $overUnder = $overUnder ?: $this->getOverUnder();

        $this->id = $this->attr('data-event-id');
        $this->status = $this->getStatus();
        $this->start_time = $this->getStartTime();
        $this->away_team_id = $awayTeamId;
        $this->home_team_id = $homeTeamId;
        $this->over_under = $overUnder;
        $this->away_money_line = $awayMoneyLine;
        $this->home_money_line = $homeMoneyLine;
        $this->away_scores = $awayScores;
        $this->home_scores = $homeScores;
        $this->consensus_href = $this->getConsensusHref();
        $this->boxscore_href = $this->getBoxScoreHref();
        $this->isLeagueGame = (bool)stristr($this->getTeamsHeader(), 'league');
    }

    /**
     * @return string[]
     */
    private function getTeamIds(): array
    {
        return array_map(function (HtmlParser $link) {
            preg_match('/team([0-9]+)\.html/', $link->attr('href'), $matches);
            return end($matches);
        }, $this->filter('*[contains(@class,"cmg_team_logo")]/a'));
    }

    /**
     * @return array[]
     */
    private function getTableData(): array
    {
        $tableData = [];
        if ($table = $this->select('*[contains(@class,"cmg_matchup_line_score")]')) {
            $tableRows = $table->filter('tbody/tr');
            foreach ($table->filter('thead/tr/th') as $i => $header) {
                $header = $header->text();
                $tableData[$header] = [
                    $tableRows[0]->filter('td')[$i]->text(),
                    $tableRows[1]->filter('td')[$i]->text(),
                ];
            }
        }
        return $tableData;
    }

    private function getStatus(): string
    {
        $status = $this->select('*[contains(@class,"cmg_matchup_list_status")]')
            ?: $this->select('*[contains(@class,"cmg_game_time")]');

        return trim(preg_replace('/\s+/', ' ', strtolower($status->text())));

    }

    private function getOverUnder(): float
    {
        return floatval($this->attr('data-game-total'));
    }

    private function getStartTime(): DateTime
    {
        return new DateTime($this->attr('data-game-date'), new DateTimeZone('America/New_York'));
    }

    private function getConsensusHref(): ?string
    {
        return ($link = $this->select('a[contains(@href,"Consensus")]')) ? $link->attr('href') : null;
    }

    private function getBoxScoreHref(): ?string
    {
        return ($link = $this->select('a[contains(@href,"boxscore")]')) ? $link->attr('href') : null;
    }

    private function getTeamsHeader(): string
    {
        return $this->select('div[contains(@class,"cmg_matchup_header_team_names")]')->text();
    }
}
