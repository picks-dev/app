<?php

namespace PickSuite\WebScraper\Covers;

use DateTime;

class Router
{
    public function teamsPage(string $sport): string
    {
        return "https://www.covers.com/pageLoader/pageLoader.aspx?page=/data/{$sport}/teams/teams.html";
    }

    public function gamesPage(string $sport, DateTime $date): string
    {
        return "https://www.covers.com/Sports/{$sport}/Matchups?selectedDate={$date->format('Y-m-d')}";
    }

    public function picksPage(string $competitionId): string
    {
        return "https://contests.covers.com/Consensus/MatchupConsensusExpertDetails/{$competitionId}";
    }

    public function teamPage(string $href)
    {
        return "https://www.covers.com{$href}";
    }

    public function consensusPage(string $consensusHref): string
    {
        return $consensusHref;
    }
}
