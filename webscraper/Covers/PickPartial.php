<?php

namespace PickSuite\WebScraper\Covers;

use PickSuite\WebScraper\Impl\DomParser;
use function strtolower;

class PickPartial extends DomParser
{
    public $game_id;
    public $total_point;
    public $total_over;
    public $expert_id;
    public $rank;
    public $team_is_away;
    public $side_is_away;

    public function __construct($html, string $gameId, int $tableIndex, string $awayName, string $awayAbbr)
    {
        parent::__construct($html);
        $this->game_id = $gameId;
        $awayName = trim(strtolower($awayName));
        $awayAbbr = trim(strtolower($awayAbbr));
        [$userLink, $text, $detail] = $this->filter('td');
        $userLink = $userLink->select('a')->attr('href');
        $text = strtolower($text->text());
        $detail = strtolower($detail->text());
        [$totalOver, $totalPoint] = $this->getTotal($tableIndex, $text);

        $this->total_point = $totalPoint;
        $this->total_over = $totalOver;
        $this->expert_id = $this->getUserId($userLink);
        $this->rank = $this->getRank($detail);
        $this->team_is_away = $this->expertIsAway($detail, $awayName);
        $this->side_is_away = $totalPoint === null ?
            $this->sideIsAway($text, $awayAbbr)
            : null;
    }

    /**
     * @param int $tableIndex
     * @param string $text
     * @return float[]|null[]
     */
    private function getTotal(int $tableIndex, string $text): array
    {
        return $tableIndex >= 2
            ? [$tableIndex === 2, floatval($text)]
            : [null, null];
    }

    private function getUserId(string $userLink): string
    {
        $hrefParts = explode('/', $userLink);
        return end($hrefParts);
    }

    private function getRank(string $detail): int
    {
        preg_match(' /#([0-9]+)/', $detail, $matches);
        return intval(end($matches));
    }

    private function expertIsAway(string $detail, $away): bool
    {
        preg_match('/on (.*) games/', $detail, $matches);
        return end($matches) === $away;
    }

    private function sideIsAway(string $text, $away): bool
    {
        return preg_replace('/[^a-z]+/', '', $text) === $away;
    }
}
