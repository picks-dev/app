<?php

namespace PickSuite\WebScraper\Covers;

use PickSuite\WebScraper\Impl\DomParser;
use function preg_match;

class ConsensusPage extends DomParser
{
    public function getCompetitionId(): string
    {
        return $this->select('*[@id="CompetitionId"]')->attr('value');
    }

    public function getAwayAbbr(): string
    {
        $awayLogo = $this->select('*[contains(@class, "covers-CoversConsensus-detailsVisitorLogo")]/a/img');
        $src = $awayLogo->attr('src');
        preg_match('/([a-z]+).gif$/', $src, $matches);
        return end($matches);
    }
}
