<?php

namespace PickSuite\WebScraper\Covers;

use PickSuite\WebScraper\HtmlParser;
use PickSuite\WebScraper\Impl\DomParser;

class GamesPage extends DomParser
{
    /**
     * @return GameBoxPartial[]
     */
    public function getGameBoxes(): array
    {
        $gameBoxes = $this->filter('*[contains(@class,"cmg_matchup_game_box")]');
        return array_map(function (HtmlParser $parser) {
            return new GameBoxPartial($parser);
        }, $gameBoxes);
    }
}
