<?php

namespace PickSuite\WebScraper\Covers;

use PickSuite\WebScraper\Impl\DomParser;
use function preg_replace;

class PicksPartial extends DomParser
{
    public $game_id;

    /**
     * @param string $awayAbbr
     * @return PickPartial[]
     */
    public function getPicks(string $awayAbbr): array
    {
        $awayName = $this->getAwayName();
        $picks = [];
        foreach ($this->filter('table') as $tableIndex => $table) {
            foreach (array_slice($table->filter('tr'), 1) as $rowIndex => $row) {
                $picks[] = new PickPartial($row, $this->game_id, $tableIndex, $awayName, $awayAbbr);
            }
        }
        return $picks;

    }

    private function getAwayName(): string
    {
        $text = $this->select('*[contains(@class,"covers-CoversConsensus-leagueHeader")]')->text();
        return preg_replace('/^.* for /', '', $text);
    }
}
