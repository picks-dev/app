<?php

namespace PickSuite\WebScraper\Covers;

use PickSuite\WebScraper\HtmlParser;
use PickSuite\WebScraper\Impl\DomParser;

class TeamsPage extends DomParser
{
    /**
     * @return string[]
     */
    public function getTeamsHrefs(): array
    {
        return array_map(function (HtmlParser $link) {
            return $link->attr('href');
        }, $this->filter('*[contains(@class,"datacell")]/a'));
    }
}
