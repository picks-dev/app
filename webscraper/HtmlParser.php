<?php

namespace PickSuite\WebScraper;

interface HtmlParser
{
    public function attr(string $name, string $default = ''): string;

    public function select(string $path): ?HtmlParser;

    /**
     * @param string $path
     * @return HtmlParser[]
     */
    public function filter(string $path): array;

    public function text(): string;
}
