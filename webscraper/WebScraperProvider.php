<?php

namespace PickSuite\WebScraper;

use Event;
use Illuminate\Support\ServiceProvider;
use PickSuite\WebScraper\Commands\ScrapeGamesCmd;
use PickSuite\WebScraper\Commands\ScrapePicksCmd;
use PickSuite\WebScraper\Commands\ScrapeTeamsCmd;
use PickSuite\WebScraper\Events\HttpRequest;
use PickSuite\WebScraper\Impl\DomParser;
use PickSuite\WebScraper\Impl\HttpTransport;
use PickSuite\WebScraper\Impl\PickScraper;
use PickSuite\WebScraper\Listeners\LogHttpRequest;

class WebScraperProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind(HtmlParser::class, DomParser::class);
        $this->app->bind(WebScraper::class, PickScraper::class);
        $this->app->bind(HttpClient::class, HttpTransport::class);
    }

    public function register()
    {
        Event::listen(HttpRequest::class, LogHttpRequest::class);
        $this->commands([
            ScrapeTeamsCmd::class,
            ScrapeGamesCmd::class,
            ScrapePicksCmd::class,
        ]);
    }
}
