<?php

namespace PickSuite\WebScraper;

interface HttpClient
{
    public function get(string $url): string;
}
