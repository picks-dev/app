<?php

namespace PickSuite\WebScraper\Listeners;

use Log;
use PickSuite\WebScraper\Events\HttpRequest;

class LogHttpRequest
{
    public function handle(HttpRequest $event)
    {
        Log::info("Fetching {$event}");
    }

}
