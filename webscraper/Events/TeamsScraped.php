<?php

namespace PickSuite\WebScraper\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use PickSuite\WebScraper\Covers\TeamPage;

class TeamsScraped
{
    use Dispatchable, SerializesModels;

    public $sport;
    public $teams;

    /**
     * @param string $sport
     * @param TeamPage[] $teams
     */
    public function __construct(string $sport, array $teams)
    {
        $this->sport = $sport;
        $this->teams = $teams;
    }
}
