<?php

namespace PickSuite\WebScraper\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class HttpRequest
{
    use Dispatchable, SerializesModels;

    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function __toString()
    {
        return $this->url;
    }
}
