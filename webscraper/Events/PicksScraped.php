<?php

namespace PickSuite\WebScraper\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use PickSuite\WebScraper\Covers\PickPartial;

class PicksScraped
{
    use Dispatchable, SerializesModels;

    public $sport;
    public $picks;
    public $recurring;

    /**
     * @param string $sport
     * @param PickPartial[] $picks
     * @param bool $recurring
     */
    public function __construct(string $sport, array $picks, bool $recurring = false)
    {
        $this->sport = $sport;
        $this->picks = $picks;
        $this->recurring = $recurring;
    }
}
