<?php

namespace PickSuite\WebScraper\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use PickSuite\WebScraper\Covers\GameBoxPartial;

class GamesScraped
{
    use Dispatchable, SerializesModels;

    public $sport;
    public $games;
    public $recurring;

    /**
     * @param string $sport
     * @param GameBoxPartial[] $games
     * @param bool $recurring
     */
    public function __construct(string $sport, array $games, bool $recurring = false)
    {
        $this->sport = $sport;
        $this->games = $games;
        $this->recurring = $recurring;
    }
}
