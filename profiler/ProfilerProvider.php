<?php

namespace PickSuite\Profiler;

use App;
use Illuminate\Support\ServiceProvider;
use URL;
use function escapeshellarg;
use function file_exists;
use function http_build_query;
use function public_path;

class ProfilerProvider extends ServiceProvider
{
    public function boot()
    {
        if (config('profiler.enabled')) {
            Profiler::start();
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config.php', 'profiler');
        if (config('profiler.enabled')) {
            App::terminating(function () {
                $xhprof = 'xhprof_html/index.php';
                if (file_exists(public_path($xhprof))) {
                    $query = Profiler::stop('picksuite');
                    $href = URL::asset($xhprof) . '?' . http_build_query($query);
                    shell_exec('php artisan serve & open ' . escapeshellarg($href));
                }
            });
        }
    }
}
