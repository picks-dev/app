<?php

return [

    'enabled' => extension_loaded('tideways_xhprof') && env('PROFILER_ENABLED'),

];

