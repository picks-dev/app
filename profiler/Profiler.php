<?php

namespace PickSuite\Profiler;

use function is_dir;
use function mkdir;
use function sys_get_temp_dir;
use function uniqid;

class Profiler
{
    public static function start(): void
    {
        tideways_xhprof_enable(
            TIDEWAYS_XHPROF_FLAGS_MEMORY |
            TIDEWAYS_XHPROF_FLAGS_MEMORY_MU |
            TIDEWAYS_XHPROF_FLAGS_MEMORY_PMU
        );
    }

    public static function stop(string $source, string $storageDir = null): array
    {
        $run = uniqid();
        $xhprofQuery = [
            'run' => $run,
            'source' => $source,
            'sort' => 'excl_wt',
        ];

        $storageDir = static::ensureDir($storageDir ?: sys_get_temp_dir());
        $targetFile = "{$storageDir}/{$run}.{$source}.xhprof";
        $data = tideways_xhprof_disable();
        file_put_contents($targetFile, serialize($data));
        return $xhprofQuery;
    }

    private static function ensureDir(string $dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        return $dir;
    }
}
