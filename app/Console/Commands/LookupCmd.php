<?php

namespace PickSuite\App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use PickSuite\App\Helpers\Arr;
use PickSuite\App\Models\Simulation;
use PickSuite\App\Models\Team;
use function array_fill;
use function serialize;
use function str_pad;

class LookupCmd extends Command
{
    protected $signature = 'picks:lookup {sport} {date?} {--teams=*} {--sides}';
    protected $description = 'Perform a series of all possible suggestions';

    public function handle()
    {
        $sport = $this->argument('sport');
        $date = ($date = $this->argument('date')) ? Carbon::parse($date) : null;
        $teams = Team::bySport($sport, ...$this->option('teams'))->pluck(Team::ATTR_ID);
        $sides = $this->option('sides');

        $simulations = Simulation
            ::query()
            ->lookup($date, $teams, $sides)
            ->get()
            ->groupBy(function (Simulation $simulation) {
                return serialize($simulation->settings);
            })
            ->map(function (Collection $simulations) {
                return $simulations
                    ->sortByDesc(function (Simulation $simulation) {
                        return $simulation->results->confidence;
                    })
                    ->first();
            })
            ->groupBy(function (Simulation $simulation) {
                return (string)$simulation->settings->targetDate;
            })
            ->map(function (Collection $simulations) {
                return $simulations
                    ->sortByDesc(function (Simulation $simulation) {
                        return $simulation->results->confidence;
                    })
                    ->first();
            });

        $headers = [
            'date',
            'teams',
            'date range',
            'mean range',
            'confidence',
            'weighted accuracy',
            'dissonance',
            'picks per day',
        ];
        $accuracy = [];
        $confidence = [];
        $data = $simulations
            ->sortByDesc(function (Simulation $simulation) {
                return $simulation->settings->targetDate->getTimestamp();
            })
            ->map(function (Simulation $simulation) use (&$accuracy, &$confidence) {
                $teams = Collection::make($simulation->results->teams)->sort()->implode(',');
                $accuracy[$teams][] = $simulation->results->accuracy;
                $confidence[$teams][] = $simulation->results->confidence;
                return [
                    $simulation->settings->targetDate,
                    $teams,
                    $simulation->settings->targetDate->diffInDays($simulation->settings->fromDate),
                    $simulation->settings->meanRange,
                    $simulation->results->confidence,
                    $simulation->results->accuracy,
                    $simulation->results->dissonance,
                    $simulation->results->picksPerDay,
                ];
            });
        $averages = [
            'Averages',
            null,
            null,
            Arr::average($data->pluck(3)),
            Arr::average($data->pluck(4)),
            Arr::average($data->pluck(5)),
            Arr::average($data->pluck(6)),
            Arr::average($data->pluck(7)),
        ];
        $data
            ->push(array_fill(0, count($headers), str_pad('', 17, '-')))
            ->push($averages);

        $this->table($headers, $data);
    }
}
