<?php

namespace PickSuite\App\Console\Commands;

use Bus;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use PickSuite\App\Jobs\PickSimulation;
use PickSuite\App\Models\Game;
use PickSuite\App\Models\Team;
use PickSuite\App\Simulation\SimulationBounds;

class SimulateCmd extends Command
{
    protected $signature = 'picks:simulate {sport} {to?} {from?} {--teams=*}';
    protected $description = 'Perform a series of all possible suggestions';

    public function handle()
    {
        $sport = $this->argument('sport');
        $toDate = Carbon::parse($this->argument('to') ?: Game::query()
            ->whereSport($sport)
            ->max(Game::ATTR_STARTS_AT));
        $fromDate = Carbon::parse($this->argument('from') ?: Game::query()
            ->whereSport($sport)
            ->min(Game::ATTR_STARTS_AT));
        $teams = Team::bySport($sport, ...$this->option('teams'));

        $bounds = new SimulationBounds($sport, $teams, $toDate, $fromDate);
        foreach ($bounds->getSettings() as $settings) {
            Bus::dispatch(new PickSimulation($settings));
        }
    }
}
