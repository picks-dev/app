<?php

namespace PickSuite\App\Console\Commands;

use Bus;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use PickSuite\App\Jobs\PickSimulation;
use PickSuite\App\Models\Team;
use PickSuite\App\Simulation\SimulationSettings;

class SuggestCmd extends Command
{
    protected
        $signature = 'picks:suggest
        {sport}
        {targetDate}
        {dateRange}
        {meanRange}
        {--sides : query sides instead of totals}
        {--deviation=0.0 : min required deviation from mean to choose a pick}
        {--majority=1 : min required majority from mean to choose a pick}
        {--teams=*}';

    protected $description = 'Suggest picks';

    public function handle()
    {
        $sport = $this->argument('sport');
        $targetDate = Carbon::parse($this->argument('targetDate'));
        $dateRange = $this->argument('dateRange');
        $meanRange = min($dateRange - 1, $this->argument('meanRange'));
        $sides = $this->option('sides');
        $teams = Team::bySport($sport, ...$this->option('teams'));
        $deviation = (float)$this->option('deviation');
        $majority = (int)$this->option('majority');

        foreach ($teams as $team) {
            $settings = new SimulationSettings(
                $sport,
                $targetDate,
                $targetDate->copy()->subDays($dateRange),
                $meanRange,
                $sides,
                $team,
                $deviation,
                $majority
            );
            Bus::dispatch(new PickSimulation($settings));
        }
    }
}
