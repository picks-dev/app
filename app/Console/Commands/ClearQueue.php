<?php

namespace PickSuite\App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Queue;
use Redis;

class ClearQueue extends Command
{
    protected $signature = 'queue:clear';

    protected $description = 'Clear all items from default queue';

    public function handle()
    {
        $conn = Queue::connection();
        $size = $conn->size();
        switch ($name = $conn->getConnectionName()) {
            case 'redis':
                Redis::flushdb();
                break;
            case 'database':
                DB::table('jobs')->delete();
                break;
            default:
                die($this->error("Connection type {$name} unhandled."));
        }
        $this->info("Cleared {$size} items.");
    }
}
