<?php

namespace PickSuite\App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use PickSuite\App\Models\Pick;
use PickSuite\App\Models\Team;
use PickSuite\App\Simulation\PickSimulation as PickSim;
use PickSuite\App\Simulation\SimulationSettings;

class PickSimulation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 2;

    private $settings;

    public function __construct(SimulationSettings $settings)
    {
        $this->settings = $settings;
    }

    public function handle()
    {
        $team = Team::query()
            ->whereSport($this->settings->sport)
            ->whereId($this->settings->team)
            ->firstOrFail();
        $picks = Pick::fetchPicks(
            [$team],
            $this->settings->targetDate,
            $this->settings->targetDate,
            $this->settings->sides
        );
        $simulation = new PickSim($this->settings, $picks);
    }
}
