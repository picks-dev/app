<?php

namespace PickSuite\App\Jobs;

use Event;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use PickSuite\App\Models\Game;
use PickSuite\WebScraper\Covers\ConsensusPage;
use PickSuite\WebScraper\Covers\PicksPartial;
use PickSuite\WebScraper\Events\PicksScraped;
use PickSuite\WebScraper\Requests\ScrapeConsensus;
use PickSuite\WebScraper\Requests\ScrapePicks as ScrapePicksRequest;
use PickSuite\WebScraper\WebScraper;
use function explode;
use function urldecode;

class ScrapePicks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sport;
    private $consensusHrefs;
    private $recurring;

    public function __construct(string $sport, Carbon $date, bool $recurring = false)
    {
        $this->sport = $sport;
        $this->consensusHrefs = $this->fetchConsensusHrefs($date);
        $this->recurring = $recurring;
    }

    private function fetchConsensusHrefs(Carbon $date): array
    {
        return Game
            ::query()
            ->with([Game::RELATION_SPORT])
            ->whereSport($this->sport)
            ->byDateRange($date)
            ->get()
            ->map(function (Game $game) {
                return $game->consensus_href;
            })
            ->toArray();
    }

    public function handle(WebScraper $webScraper)
    {
        $picks = array_reduce($this->consensusHrefs, function (array $picks, string $consensusHref) use ($webScraper) {
            // Scrape Consensus
            $consensusReq = new ScrapeConsensus($consensusHref);
            $consensusHtml = $webScraper->execute($consensusReq);
            $consensusPage = new ConsensusPage($consensusHtml);

            // Scrape picks
            $picksRequest = new ScrapePicksRequest($consensusPage->getCompetitionId());
            $picksHtml = $webScraper->execute($picksRequest);
            $picksPage = new PicksPartial($picksHtml);
            $picksPage->game_id = $this->parseGameId($consensusHref);
            return array_merge($picks, $picksPage->getPicks($consensusPage->getAwayAbbr()));
        }, []);

        Event::dispatch(new PicksScraped($this->sport, $picks, $this->recurring));
    }

    private function parseGameId(string $consensusHref): string
    {
        $consensusHref = urldecode($consensusHref);
        $parts = explode(':', $consensusHref);
        return end($parts);
    }
}
