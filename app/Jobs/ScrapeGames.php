<?php

namespace PickSuite\App\Jobs;

use DateTime;
use Event;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use PickSuite\App\Models\Sport;
use PickSuite\WebScraper\Covers\GamesPage;
use PickSuite\WebScraper\Events\GamesScraped;
use PickSuite\WebScraper\Requests\ScrapeGames as ScrapeGamesRequest;
use PickSuite\WebScraper\WebScraper;
use function array_key_exists;

class ScrapeGames implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sport;
    private $date;
    private $recurring;

    public function __construct(string $sport, DateTime $date, bool $recurring = false)
    {
        $this->sport = $sport;
        $this->date = $date;
        $this->recurring = $recurring;
    }

    public function handle(WebScraper $webScraper)
    {
        if (!array_key_exists($this->sport, Sport::SPORTS)) {
            return Log::error("Unable to scrape games for sport ({$this->sport}).");
        }
        $gamesReq = new ScrapeGamesRequest($this->sport, $this->date);
        $gamesHtml = $webScraper->execute($gamesReq);
        $gamesPage = new GamesPage($gamesHtml);

        $games = $gamesPage->getGameBoxes();

        Event::dispatch(new GamesScraped($this->sport, $games, $this->recurring));
    }
}
