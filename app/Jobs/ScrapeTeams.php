<?php

namespace PickSuite\App\Jobs;

use Event;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use PickSuite\App\Models\Sport;
use PickSuite\WebScraper\Covers\TeamPage;
use PickSuite\WebScraper\Covers\TeamsPage;
use PickSuite\WebScraper\Events\TeamsScraped;
use PickSuite\WebScraper\Requests\ScrapeTeam;
use PickSuite\WebScraper\Requests\ScrapeTeams as ScrapeTeamsRequest;
use PickSuite\WebScraper\WebScraper;
use function array_map;

class ScrapeTeams implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sport;

    public function __construct(string $sport)
    {
        $this->sport = $sport;
    }

    public function handle(WebScraper $webScraper)
    {
        if (!array_key_exists($this->sport, Sport::SPORTS)) {
            return Log::error("Unable to scrape teams for sport ({$this->sport}).");
        }
        $teamsReq = new ScrapeTeamsRequest($this->sport);
        $teamsHtml = $webScraper->execute($teamsReq);
        $teamsPage = new TeamsPage($teamsHtml);

        $teams = array_map(function (string $href) use ($webScraper) {
            $teamReq = new ScrapeTeam($href);
            $teamHtml = $webScraper->execute($teamReq);
            return new TeamPage($teamHtml);
        }, $teamsPage->getTeamsHrefs());

        Event::dispatch(new TeamsScraped($this->sport, $teams));
    }
}
