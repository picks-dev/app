<?php

namespace PickSuite\App\Builders;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use PickSuite\App\Models\Game;
use PickSuite\App\Models\Pick;
use PickSuite\App\Models\Team;

class TeamBuilder extends Builder
{
    public function whereSport(string $sportId)
    {
        return $this->whereHas(Game::RELATION_SPORT, function (Builder $builder) use ($sportId) {
            return $builder->whereId($sportId);
        });
    }
}
