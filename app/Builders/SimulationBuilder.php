<?php

namespace PickSuite\App\Builders;

use Illuminate\Support\Carbon;
use PickSuite\App\Models\Simulation;

class SimulationBuilder extends Builder
{
    public function lookup(?Carbon $date, iterable $teams = [], bool $sides = false): Builder
    {
        return ($date ? $this->where(Simulation::ATTR_TARGET_DATE, (string)$date) : $this)
            ->where(Simulation::ATTR_SIDES, (int)$sides)
            ->whereIn(Simulation::ATTR_TEAM_ID, $teams);
    }
}
