<?php

namespace PickSuite\App\Builders;

use ArrayAccess;
use Illuminate\Support\Collection;
use PickSuite\App\Models\Model;
use function array_reduce;
use function escapeshellarg;
use function is_numeric;
use function preg_replace;

class Builder extends \Illuminate\Database\Eloquent\Builder
{
    public function whereId(string ...$ids)
    {
        return $this->where(function (Builder $builder) use ($ids) {
            return $builder
                ->whereIn(Model::ATTR_ID, $ids)
                ->orWhereIn(Model::ATTR_UUID, $ids);
        });
    }

    public function getFromChunksOfSize(int $count, ArrayAccess $into = null): ArrayAccess
    {
        $into = $into ?: Collection::make();

        $this->chunk($count, function (iterable $chunk) use ($into) {
            foreach ($chunk as $item) {
                $into[] = $item;
            }
        });

        return $into;
    }

    public function chunk($count, callable $callback)
    {
        $max = $this->query->limit ?? $this->query->unionLimit ?? null;
        $total = 0;
        $this->enforceOrderBy();

        $page = 1;

        do {
            $results = ($max && $total + $count > $max)
                ? $this->limit($max - $total)->offset($total)->get()
                : $results = $this->forPage($page, $count)->get();
            // We'll execute the query for the given page and get the results. If there are
            // no results we can just break and return from here. When there are results
            // we will call the callback with the current chunk of these results here.

            $total += $countResults = $results->count();

            if ($countResults == 0) {
                break;
            }

            // On each chunk result set, we will pass them to the callback and then let the
            // developer take care of everything within the callback, which allows us to
            // keep the memory low for spinning through large result sets for working.
            if ($callback($results, $page) === false) {
                return false;
            }

            unset($results);

            $page++;
        } while ($countResults == $count && (!$max || $total < $max));

        return true;
    }

    public function toSql()
    {
        return array_reduce($this->getBindings(), function (string $sql, $binding) {
            if (!is_numeric($binding)) {
                $binding = escapeshellarg($binding);
            }
            return preg_replace('/\?/', $binding, $sql, 1);
        }, parent::toSql());
    }
}
