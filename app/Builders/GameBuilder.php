<?php

namespace PickSuite\App\Builders;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use PickSuite\App\Models\Game;
use PickSuite\App\Models\Pick;
use PickSuite\App\Models\Team;

class GameBuilder extends Builder
{
    public function forSuggestion(array $teams, bool $sides)
    {
        return $this
            ->with([
                Game::RELATION_SPORT,
                Game::RELATION_SCORES,
                Game::RELATION_PICKS,
                Game::RELATION_AWAY,
                Game::RELATION_HOME,
                Game::RELATION_PICKS . '.' . Pick::RELATION_TEAM,
                Game::RELATION_PICKS . '.' . Pick::RELATION_EXPERT,
                Game::RELATION_PICKS . '.' . Pick::RELATION_SIDE,
            ])
            ->whereHas(Game::RELATION_PICKS, function (Builder $builder) use ($sides, $teams) {
                $builder = ($sides
                    ? $builder->whereNull(Pick::ATTR_TOTAL_POINT)
                    : $builder->whereNotNull(Pick::ATTR_TOTAL_POINT));

                if ($teams) {
                    $builder = $builder->whereHas(Pick::RELATION_TEAM, function (Builder $builder) use ($teams) {
                        return $builder
                            ->whereId(...$teams)
                            ->orWhereIn(Team::ATTR_ABBR, $teams);
                    });
                }

                return $builder;
            })
            ->with([
                Game::RELATION_PICKS => function (HasMany $hasMany) use ($sides, $teams) {
                    $hasMany = ($sides
                        ? $hasMany->whereNull(Pick::ATTR_TOTAL_POINT)
                        : $hasMany->whereNotNull(Pick::ATTR_TOTAL_POINT));

                    if ($teams) {
                        $hasMany = $hasMany->whereHas(Pick::RELATION_TEAM, function (Builder $builder) use ($teams) {
                            return $builder
                                ->whereId(...$teams)
                                ->orWhereIn(Team::ATTR_ABBR, $teams);
                        });
                    }

                    return $hasMany;
                },
            ])
            ->orderBy(Game::ATTR_STARTS_AT);
    }

    public function byDateRange(Carbon $to, ?Carbon $from = null)
    {
        $from = $from ?? $to->copy();

        return $this
            ->whereDate(Game::ATTR_STARTS_AT, '<=', $to->endOfDay())
            ->whereDate(Game::ATTR_STARTS_AT, '>=', $from->startOfDay());
    }

    public function whereSport(string $sportId)
    {
        return $this->whereHas(Game::RELATION_SPORT, function (Builder $builder) use ($sportId) {
            return $builder->whereId($sportId);
        });
    }
}
