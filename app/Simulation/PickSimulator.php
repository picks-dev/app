<?php

namespace PickSuite\App\Simulation;

use Illuminate\Support\Carbon;
use PickSuite\App\Helpers\Arr;
use PickSuite\App\Models\Pick;
use PickSuite\App\Models\Team;

class PickSimulator
{
    public function simulate(string $sport, Carbon $to, array $teams, bool $sides): array
    {
        $bounds = new SimulationBounds($sport, $teams, $to, $to);
        $from = $bounds->toDate->copy()->subDays($bounds->maxDateRange);
        $picks = Pick::fetchPicks(Arr::pluck($teams, Team::ATTR_ID), $from, $to, $sport);

        $results = [];
        foreach ($bounds->getSettings() as $settings) {
            if ($settings->sides !== $sides) {
                continue;
            }
            $targetPicks = array_slice(
                $picks[$settings->team][$settings->sides ? 's' : 't'],
                -1 * $settings->dateRange
            );
            $simulation = new PickSimulation($settings, $targetPicks);
            $results[] = $simulation;
        }
        return $results;
    }

    public function findBest(iterable $simulations): PickSimulation
    {
        return Arr::reduce($simulations, function (PickSimulation $a, PickSimulation $b) {
            return $a->isBetterThan($b) ? $a : $b;
        }, $simulations[0]);
    }
}
