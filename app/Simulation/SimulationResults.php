<?php

namespace PickSuite\App\Simulation;

use function serialize;

class SimulationResults
{
    public $hasPicksOnTarget;
    public $confidence;
    public $accuracy;
    public $dissonance;
    public $picksPerDay;
    public $teams;

    public function __construct(
        bool $hasPicksOnTarget,
        ?float $confidence,
        ?float $accuracy,
        float $picksPerDay,
        array $teams
    ) {
        $this->hasPicksOnTarget = $hasPicksOnTarget;
        $this->confidence = $confidence;
        $this->accuracy = $accuracy;
        $this->dissonance = $this->confidence === null || $this->accuracy === null
            ? null
            : $accuracy - $confidence;
        $this->picksPerDay = $picksPerDay;
        $this->teams = $teams;
        sort($this->teams);
    }

    public function __toString()
    {
        return serialize($this);
    }
}
