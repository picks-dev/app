<?php

namespace PickSuite\App\Simulation;

use Illuminate\Support\Carbon;
use PickSuite\App\Models\Team;
use Traversable;

class SimulationBounds
{
    public $sport;
    /** @var iterable|Team[] */
    public $teams;
    public $toDate;
    public $fromDate;

    public $minDateRange = 30;
    public $maxDateRange = 90;
    public $dateRangeIncrement = 15;

    public $minMeanRange = 3;
    public $maxMeanRange = 30;
    public $meanRangeIncrement = 3;

    public $minMeanDeviation = 0.00;
    public $maxMinMeanDeviation = 0.30;
    public $meanDeviationIncrement = 0.05;

    public $minMajority = 1;
    public $maxMajority = 3;

    public $minMinPicksPerDay = 0.5;
    public $maxMinPicksPerDay = 1.5;
    public $minPicksPerDayIncrement = 0.1;

    public function __construct(string $sport, iterable $teams, Carbon $toDate, Carbon $fromDate = null)
    {
        $this->sport = $sport;
        $this->teams = $teams;
        $this->toDate = $toDate;
        $this->fromDate = $fromDate ?? $toDate->copy();
    }

    /**
     * @return Traversable|SimulationSettings[]
     */
    public function getSettings(): Traversable
    {
        $targetDate = $this->fromDate->copy();
        while ($targetDate->lte($this->toDate)) {
            for ($dateRange = $this->minDateRange; $dateRange <= $this->maxDateRange; $dateRange += $this->dateRangeIncrement) {
                for ($meanRange = $this->minMeanRange; $meanRange <= $this->maxMeanRange; $meanRange += $this->meanRangeIncrement) {
                    foreach ([false, true] as $sides) {
                        foreach ($this->teams as $team) {
                            for ($minMeanDeviation = $this->minMeanDeviation; $minMeanDeviation <= $this->maxMinMeanDeviation; $minMeanDeviation += $this->meanDeviationIncrement) {
                                for ($minMajority = $this->minMajority; $minMajority <= $this->maxMajority; $minMajority++) {
                                    for ($minPicksPerDay = $this->minMinPicksPerDay; $minPicksPerDay <= $this->maxMinPicksPerDay; $minPicksPerDay += $this->minPicksPerDayIncrement) {
                                        yield new SimulationSettings(
                                            $this->sport,
                                            $targetDate->copy(),
                                            $dateRange,
                                            $meanRange,
                                            $sides,
                                            $team,
                                            $minMeanDeviation,
                                            $minMajority,
                                            $minPicksPerDay
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $targetDate->addDay();
        }
    }
}
