<?php

namespace PickSuite\App\Simulation;

use PickSuite\App\Helpers\Arr;
use PickSuite\App\Models\Pick;
use function array_filter;
use function array_push;

class PickSimulation
{
    private $settings;
    private $picks;
    private $weightedAccuracy;
    private $predictions;
    private $targetDate;
    private $picksPerDay;

    public function __construct(SimulationSettings $settings, array $picks)
    {
        $this->settings = $settings;
        $this->picks = $picks;
    }

    public function isBetterThan(PickSimulation $other)
    {
        if (!$this->hasPicksOnTarget()) {
            return false;
        }
        if (!$other->hasPicksOnTarget()) {
            return true;
        }
        $thisConfidence = round($this->getConfidence(), 5);
        $thatConfidence = round($other->getConfidence(), 5);
        if ($thisConfidence > $thatConfidence) {
            return true;
        }
        if ($thisConfidence === $thatConfidence) {
            return $this->getPicksPerDay() >= $other->getPicksPerDay();
        }
        return false;
    }

    public function hasPicksOnTarget(): bool
    {
        return (bool)($this->getPredictions()[(string)$this->settings->targetDate] ?? false);
    }

    public function getPredictions()
    {
        if (isset($this->predictions)) {
            return $this->predictions;
        }
        $winsPerPick = $this->calculateWinsPerPick($this->picks);
        $means = $this->calculateMeans($winsPerPick);
        $deviations = $this->calculateDeviations($means, $winsPerPick);
        $streaks = $this->calculateStreaks($winsPerPick);

        $predictions = [];
        foreach ($means as $date => $mean) {
            $winsPerPick = $winsPerPick[$date];
            $streak = $streaks[$date];
            $deviation = $deviations[$date];
            foreach ($this->picks[$date] as $summary => $pick) {
                $predictions[$date][$summary] = $this->shouldWin($pick, $mean, $deviation, $streak);
            }
        }
        $this->picksPerDay = Arr::average(Arr::map($predictions, function (array $predictions) {
            return count(array_filter($predictions, function (?bool $prediction) {
                return $prediction !== null;
            }));
        }));
        if ($this->picksPerDay < $this->settings->minPicksPerDay) {
            $predictions = [];
        }
        return $this->predictions = $predictions;
    }

    private function calculateWinsPerPick(array $picks): array
    {
        $previousWinsPerPick = null;
        return Arr::map($picks, function (array $picks) use (&$previousWinsPerPick) {
            $result = $previousWinsPerPick;
            $previousWinsPerPick = Arr::average(Arr::reduce($picks, function (array $acc, Pick $pick) {
                return array_merge($acc, array_fill(0, $pick->weight, $pick->isCorrect));
            }, []));
            return $result;
        });
    }

    private function calculateMeans(array $winsPerPick): array
    {
        $tmp = [];
        return Arr::map($winsPerPick, function (?float $winsPerPick) use (&$tmp) {
            array_push($tmp, $winsPerPick);
            $tmpCount = count($tmp);
            if ($tmpCount > $this->settings->meanRange) {
                array_shift($tmp);
            }
            return $tmpCount === $this->settings->meanRange + 1
                ? Arr::average($tmp)
                : null;

        });
    }

    private function calculateDeviations(array $means, array $winsPerPick): array
    {
        return Arr::map($means, function (?float $mean, string $date) use ($winsPerPick) {
            $wpp = $winsPerPick[$date] ?? null;

            if ($mean === null || $wpp === null) {
                return null;
            }

            return $wpp - $mean;
        });
    }

    private function calculateStreaks(array $winsPerPick): array
    {
        $streak = 0;
        return Arr::map($winsPerPick, function (?float $winsPerPick) use (&$streak) {
            if ($winsPerPick && $winsPerPick > 0.5) {
                return $streak = $streak < 0 ? 1 : $streak + 1;
            }
            if ($winsPerPick && $winsPerPick < 0.5) {
                return $streak = $streak > 0 ? -1 : $streak - 1;
            }
            return $streak = null;
        });
    }

    private function shouldWin(Pick $pick, ?float $mean, ?float $deviation, ?int $streak): ?bool
    {
        if ($deviation === null || abs($deviation) < $this->settings->minMeanDeviation || abs($pick->margin) < $this->settings->minMajority) {
            return null;
        }

        // If on a winning streak, agree with them
        if ($streak > 1) {
            return $pick->margin > 0;
        }
        // If on a losing streak, contradict them
        if ($streak < -1) {
            return $pick->margin < 0;
        }

        // they're doing worse than usual, agree with them
        if ($mean && $mean < 0.5 && $deviation && $deviation < 0) {
            return $pick->margin > 0;
        }
        // they're doing better than usual, contradict them
        if ($mean && $mean > 0.5 && $deviation && $deviation > 0) {
            return $pick->margin < 0;
        }

        return null;
    }

    public function getConfidence(): ?float
    {
        return Arr::average(Arr::except($this->getWeightedAccuracy(), (string)$this->settings->targetDate));
    }

    private function getWeightedAccuracy(): array
    {
        return $this->weightedAccuracy
            = $this->getWeightedAccuracy ?? Arr::map($this->getPredictions(), function (array $picks, string $date) {
                return Arr::average(Arr::reduce($picks, function (array $acc, ?bool $prediction, string $summary) use (
                    $date
                ) {
                    $pick = $this->picks[$date][$summary];
                    $isCorrect = $pick->isCorrect;
                    $result = $prediction === null || $isCorrect === null ? null : $prediction === $isCorrect;
                    return array_merge($acc, array_fill(0, abs($pick->margin), $result));
                }, []));
            });
    }

    private function getPicksPerDay(): float
    {
        if (!isset($this->picksPerDay)) {
            $this->getPredictions();
        }
        return $this->picksPerDay;
    }

    public function getAccuracy(): ?float
    {
        return $this->getWeightedAccuracy()[(string)$this->settings->targetDate] ?? null;
    }

    public function getSettings(): SimulationSettings
    {
        return $this->settings;
    }

    public function getPick(string $date, string $summary): Pick
    {
        return $this->picks[$date][$summary];
    }
}
