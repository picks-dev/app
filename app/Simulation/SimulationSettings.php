<?php

namespace PickSuite\App\Simulation;

use Illuminate\Support\Carbon;
use PickSuite\App\Models\Team;
use function serialize;

class SimulationSettings
{
    public $sport;
    public $targetDate;
    public $dateRange;
    public $meanRange;
    public $sides;
    public $team;
    public $minMeanDeviation;
    public $minMajority;
    public $minPicksPerDay;

    public function __construct(
        string $sport,
        Carbon $targetDate,
        int $dateRange,
        int $meanRange,
        bool $sides,
        Team $team,
        float $minMeanDeviation,
        int $minMajority,
        float $minPicksPerDay
    ) {
        $this->sport = $sport;
        $this->targetDate = $targetDate;
        $this->dateRange = $dateRange;
        $this->meanRange = $meanRange;
        $this->sides = $sides;
        $this->team = $team->id;
        $this->minMeanDeviation = $minMeanDeviation;
        $this->minMajority = $minMajority;
        $this->minPicksPerDay = $minPicksPerDay;
    }

    public function __toString()
    {
        return serialize($this);
    }
}
