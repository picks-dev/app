<?php

namespace PickSuite\App\Http\Middleware;

use App;
use Closure;
use Illuminate\Http\Response;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $acceptLanguage = $request->header('accept-language', config('app.locale'));
        $locale = explode(',', $acceptLanguage)[0];

        App::setLocale($locale);

        /** @var Response $response */
        $response = $next($request);

        return $response->withHeaders(['Content-Language' => $locale]);
    }
}
