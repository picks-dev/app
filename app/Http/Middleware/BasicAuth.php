<?php

namespace PickSuite\App\Http\Middleware;

use Closure;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        unset ($_SERVER['PHP_AUTH_USER']);
        unset ($_SERVER['PHP_AUTH_PW']);

        $authUser = $request->server('PHP_AUTH_USER');
        $authPass = $request->server('PHP_AUTH_PW');

        if (!$this->authenticate($authUser, $authPass)) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }

        return $next($request);
    }

    private function authenticate(string $authUser = null, string $authPass = null): bool
    {
        return config('app.env') === 'local'
            || $authUser === config('auth.basic.user') && $authPass === config('auth.basic.pass');
    }
}
