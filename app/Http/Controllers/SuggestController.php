<?php

namespace PickSuite\App\Http\Controllers;

use Illuminate\Support\Collection;
use PickSuite\App\Helpers\Arr;
use PickSuite\App\Helpers\Str;
use PickSuite\App\Http\Requests\SuggestionRequest;
use PickSuite\App\Models\Sport;
use PickSuite\App\Models\Team;
use PickSuite\App\Simulation\PickSimulation;
use PickSuite\App\Simulation\PickSimulator;
use PickSuite\App\Views\Components\Table;
use PickSuite\App\Views\Forms\SuggestForm;
use PickSuite\App\Views\Pages\SuggestionPage;
use function array_merge;
use function array_reverse;
use function array_slice;
use function strtoupper;

class SuggestController extends Controller
{
    public function __invoke(PickSimulator $simulator, SuggestionRequest $request)
    {
        $form = $this->buildSuggestionForm($request);
        $tables = Collection
            ::make($simulator->simulate($request->sport, $request->to, $request->teams->all(), $request->sides))
            ->groupBy(function (PickSimulation $sim) {
                return $sim->getSettings()->team;
            })
            ->map(function (iterable $sims) use ($simulator) {
                return $simulator->findBest($sims);
            })
            ->keyBy(function (PickSimulation $sim) use ($request) {
                $teamLabel = strtoupper($request->teams->first(function (Team $team) use ($sim) {
                    return $team->id === $sim->getSettings()->team;
                })->{Team::ATTR_ABBR});
                $confidence = $sim->getConfidence();
                $confidence = $confidence === null ? '...' : Str::percent($confidence);
                $accuracy = $sim->getAccuracy();
                $accuracy = $accuracy === null ? '...' : Str::percent($accuracy);
                return "{$teamLabel} - Confidence: {$confidence} | Accuracy: {$accuracy}";
            })
            ->map(function (PickSimulation $sim) use ($request) {
                $headers = ['Pick', 'Expected', 'Units', 'Result', 'Unit Change'];
                $predictions = array_slice($sim->getPredictions(), -1);
                $data = Arr::reduce($predictions, function (array $data, array $picks, string $date) use ($sim) {
                    $picks = Arr::map($picks, function (?bool $prediction, string $summary) use ($sim, $date) {
                        $pick = $sim->getPick($date, $summary);
                        $units = $prediction === null ? 0 : abs($pick->margin);
                        $isCorrect = $pick->isCorrect;
                        if ($prediction === false) {
                            $summary = $pick->invert()->summary;
                            $prediction = !$prediction;
                            $isCorrect = $isCorrect === null ? null : !$isCorrect;
                        }

                        return [
                            $summary,
                            [true => 'to Win', false => 'to Lose', null => 'to Skip'][$prediction],
                            "x{$units}",
                            [true => 'did Win', false => 'did Lose', null => 'is Pending'][$isCorrect],
                            $isCorrect === null ? '...' : ($units * ($prediction === $isCorrect ? 1 : -1)),
                        ];
                    });
                    return array_merge($data, $picks);
                }, []);
                return new Table($headers, array_reverse($data));
            });

        return new SuggestionPage($form, $tables);
    }

    private function buildSuggestionForm(SuggestionRequest $request): SuggestForm
    {
        return (new SuggestForm($request))
            ->setSportOptions(Sport::query()->get())
            ->setTeamsOptions(Team::bySport($request->sport));
    }
}
