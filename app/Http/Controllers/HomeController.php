<?php

namespace PickSuite\App\Http\Controllers;

use Illuminate\Support\Collection;
use PickSuite\App\Helpers\Arr;
use PickSuite\App\Http\Requests\GamesRequest;
use PickSuite\App\Models\Game;
use PickSuite\App\Views\Components\LinkDeck;
use PickSuite\App\Views\Components\ScrapeTimesHeader;
use PickSuite\App\Views\Pages\GenericPage;
use URL;
use function array_merge;
use function sprintf;
use function strtoupper;

class HomeController extends Controller
{
    public function __invoke(GamesRequest $request)
    {
        $decks = Game
            ::query()
            ->with([
                Game::RELATION_SPORT,
                Game::RELATION_AWAY,
                Game::RELATION_HOME,
            ])
            ->byDateRange($request->date)
            ->orderByDesc(Game::ATTR_STARTS_AT)
            ->get()
            ->groupBy(function (Game $game) {
                return $game->sport->abbr;
            });
        $decks = Arr::reduce($decks, function (array $acc, Collection $games, string $sport) {
            $header = new ScrapeTimesHeader($sport);
            $deck = new LinkDeck($games
                ->keyBy(function (Game $game) {
                    return sprintf(
                        '%s vs %s @ %s',
                        strtoupper($game->away->abbr),
                        strtoupper($game->home->abbr),
                        $game->starts_at->format('h:i a')
                    );
                })
                ->map(function (Game $game) {
                    $url = URL::route(SuggestController::class, [
                        'sport' => $game->sport->uuid,
                        'to' => (string)$game->starts_at,
                        'teams' => [$game->away->uuid, $game->home->uuid],
                    ]);
                    $attributes = ['target' => '_blank'];
                    return [$url, $attributes];
                }));
            return array_merge($acc, [$header, $deck]);
        }, []);
        return new GenericPage(...$decks);
    }
}
