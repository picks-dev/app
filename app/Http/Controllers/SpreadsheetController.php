<?php

namespace PickSuite\App\Http\Controllers;

use Bus;
use PickSuite\App\Helpers\Arr;
use PickSuite\App\Http\Requests\SpreadsheetRequest;
use PickSuite\App\Jobs\ScrapeGames;
use PickSuite\App\Jobs\ScrapePicks;
use PickSuite\App\Models\Game;
use PickSuite\App\Models\Pick;
use PickSuite\App\Models\Sport;
use PickSuite\App\SpreadsheetPicks;
use PickSuite\App\Views\Components\Table;
use PickSuite\App\Views\Forms\SpreadsheetForm;
use PickSuite\App\Views\Pages\GenericPage;
use Redirect;

class SpreadsheetController extends Controller
{
    public function __invoke(SpreadsheetRequest $request)
    {
        if ($request->has("scrape")) {
            Bus::dispatchNow(new ScrapeGames($request->sport, $request->to, false));
            Bus::dispatchNow(new ScrapePicks($request->sport, $request->to, false));
            return Redirect::route(SpreadsheetController::class, Arr::except($request->query(), ['scrape']));
        }

        $sports = Sport::query()
            ->pluck(Sport::ATTR_ABBR, Sport::ATTR_UUID);
        $form = (new SpreadsheetForm($request))
            ->setSportOptions($sports);
        $sheets = Game
            ::query()
            ->with([
                Game::RELATION_AWAY,
                Game::RELATION_HOME,
                Game::RELATION_SCORES,
                Game::RELATION_PICKS . '.' . Pick::RELATION_TEAM,
                Game::RELATION_PICKS . '.' . Pick::RELATION_SIDE,
            ])
            ->whereSport($request->sport)
            ->byDateRange($request->to, $request->from)
            ->get()
            ->map(function (Game $game) {
                return SpreadsheetPicks::fromGame($game);
            });

        return new GenericPage($form, new Table(SpreadsheetPicks::HEADERS, $sheets));
    }
}
