<?php

namespace PickSuite\App\Http\Requests;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use PickSuite\App\Models\Team;

/**
 * @property string $sport
 * @property Collection|Team[] $teams
 * @property Carbon $to
 * @property int $maxMeanRange
 * @property int $meanRangeIncrement
 * @property float $maxMinDev
 * @property float $minDevIncrement
 * @property int $minDateRange
 * @property int $maxDateRange
 * @property int $dateRangeIncrement
 * @property int $minPicksPerDay
 * @property bool $sides
 */
class SuggestionRequest extends FormRequest
{
    const DEFAULT_SPORT = 'mlb';
    const DEFAULT_MAX_MIN_DEV = 0.30;
    const DEFAULT_MIN_PPD = 1;
    const DEFAULT_MAX_MEAN_RANGE = 60;
    const DEFAULT_MEAN_RANGE_INCREMENT = 2;
    const DEFAULT_MIN_DEV_INCREMENT = 0.02;
    const DEFAULT_MIN_DATE_RANGE = 10;
    const DEFAULT_MAX_DATE_RANGE = 40;

    public function rules()
    {
        return [
//            'to' => ['date'],
//            'from' => ['before_or_equal:to'],
            'maxMeanRange' => ['min:1', 'integer'],
            'meanRangeIncrement' => ['min:1', 'integer'],
            'maxMinDev' => ['min:0', 'numeric'],
            'minDevIncrement' => ['min:0.01', 'numeric'],
            'minDateRange' => ['min:1', 'integer'],
            'maxDateRange' => ['min:1', 'integer'],
            'dateRangeIncrement' => ['min:1', 'integer'],
            'minPicksPerDay' => ['min:0.01', 'numeric'],
        ];
    }

    protected function getSportField(?string $sport): string
    {
        return $sport ?? static::DEFAULT_SPORT;
    }

    protected function getTeamsField($teams = null): Collection
    {
        return Team::bySport($this->sport, ...($teams ?: []));
    }

    protected function getToField(?string $to): Carbon
    {
        return Carbon::parse($to ?? 'now')->endOfDay();
    }

    protected function getSidesField(): bool
    {
        return $this->has('sides');
    }
}
