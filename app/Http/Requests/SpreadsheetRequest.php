<?php

namespace PickSuite\App\Http\Requests;

use Illuminate\Support\Carbon;

/**
 * @property string $sport
 * @property Carbon $to
 * @property Carbon $from
 */
class SpreadsheetRequest extends FormRequest
{
    const DEFAULT_SPORT = 'mlb';

    protected function getSportField(?string $sport): string
    {
        return $sport ?? static::DEFAULT_SPORT;
    }

    protected function getToField(?string $to): Carbon
    {
        return Carbon::parse($to ?? 'now')->endOfDay();
    }

    protected function getFromField(?string $from): Carbon
    {
        return Carbon::parse($from ?? $this->to)->startOfDay();
    }
}
