<?php

namespace PickSuite\App\Http\Requests;

use Illuminate\Support\Carbon;

/**
 * @property String $sport
 * @property Carbon $date
 */
class GamesRequest extends FormRequest
{
    protected function getSportField(?string $sport): string
    {
        return $sport ?? 'mlb';
    }

    protected function getDateField(?string $date): Carbon
    {
        return Carbon::parse($date ?? 'now');
    }
}
