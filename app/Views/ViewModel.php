<?php

namespace PickSuite\App\Views;

use View;
use PickSuite\App\Helpers\Str;

abstract class ViewModel
{
    protected static $CSS = [];
    protected static $JS = [];

    protected $template;

    public function __construct(string $template)
    {
        $this->template = $template;
    }

    public function __toString()
    {
        return Str::consumeException(function () {
            return View::make($this->template, ['vm' => $this])->render();
        });
    }

    public function pushJs(string ...$srcs)
    {
        foreach ($srcs as $src) {
            static::$JS[$src] = $src;
        }
        return $this;
    }

    public function pushCss(string ...$links)
    {
        foreach ($links as $link) {
            static::$CSS[$link] = $link;
        }
        return $this;
    }

    public function getCss(): array
    {
        return static::$CSS;
    }

    public function getJs(): array
    {
        return static::$JS;
    }
}
