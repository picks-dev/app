<?php

namespace PickSuite\App\Views\Components;

use function asset;
use PickSuite\App\Helpers\Str;
use PickSuite\App\Views\ViewModel;

class Table extends ViewModel
{
    private $headers;
    private $data;

    public function __construct(iterable $headers, iterable $data)
    {
        parent::__construct('components.table');
        static::pushJs(asset('js/copyable.js'));
        $this->headers = $headers;
        $this->data = $data;
    }

    public function getHeaders(): iterable
    {
        return $this->headers;
    }

    public function getRows(): iterable
    {
        return $this->data;
    }

    public function toCsv(): string
    {
        return Str::print_r($this->data);
    }
}
