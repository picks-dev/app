<?php

namespace PickSuite\App\Views\Components;

use Illuminate\Support\Carbon;
use PickSuite\App\Helpers\Arr;
use PickSuite\App\Helpers\Meta;
use PickSuite\App\Views\ViewModel;
use function strtoupper;

class ScrapeTimesHeader extends ViewModel
{
    private $sport;

    public function __construct(string $sport)
    {
        parent::__construct('components.scrape-times-header');
        $this->sport = $sport;
    }

    public function getScrapeTimes(): iterable
    {
        $data = [
            'Teams' => Meta::getTeamsScrapeTimes($this->sport),
            'Games' => Meta::getGamesScrapeTimes($this->sport),
            'Picks' => Meta::getPicksScrapeTimes($this->sport),
        ];
        return Arr::map($data, function (array $dates, string $label) {
            $sport = $this->getBrand();
            /**
             * @var Carbon $last
             * @var Carbon $next
             */
            [$last, $next] = $dates;
            $last = $last
                ? "{$sport} {$label} last scraped {$last->diffForHumans()}"
                : "{$sport} {$label}' last scrape time unknown";
            $next = $next
                ? "next scrape in {$next->diffInMinutes()} minutes"
                : "next scrape time unknown";

            return "{$last}, {$next}.";
        });
    }

    public function getBrand(): string
    {
        return strtoupper($this->sport);
    }

    public function getHref(): string
    {
        return "https://www.covers.com/sports/{$this->sport}/matchups";
    }
}
