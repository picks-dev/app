<?php

namespace PickSuite\App\Views\Components;

use PickSuite\App\Helpers\Arr;
use PickSuite\App\Views\HasLinks;
use PickSuite\App\Views\ViewModel;

class Navigation extends ViewModel
{
    use HasLinks;

    private $orientationClass;

    public function __construct(iterable $links)
    {
        parent::__construct('components.navigation');
        $this->processLinks($links);
        $this->horizontal();
    }

    public function horizontal(): Navigation
    {
        $this->orientationClass = 'justify-content-center';
        return $this;
    }

    public function vertical(): Navigation
    {
        $this->orientationClass = 'flex-column';
        return $this;
    }

    public function getOrientation(): string
    {
        return $this->orientationClass;
    }

    private function attributesToString(array $attributes): string
    {
        return Arr::reduce($attributes, function (string $acc, $value, $attribute) {
            return $acc . " {$attribute}=\"{$value}\"";
        }, '');
    }
}
