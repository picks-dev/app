<?php

namespace PickSuite\App\Views\Components;

use PickSuite\App\Views\HasLinks;
use PickSuite\App\Views\ViewModel;
use function strtoupper;

class LinkDeck extends ViewModel
{
    use HasLinks;

    private $label;

    public function __construct(iterable $links)
    {
        parent::__construct('components.link-deck');
        $this->processLinks($links);
    }
}
