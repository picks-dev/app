<?php

namespace PickSuite\App\Views\Components;

use Config;
use PickSuite\App\Http\Controllers\HomeController;
use PickSuite\App\Http\Controllers\SpreadsheetController;
use PickSuite\App\Views\ViewModel;
use Queue;
use URL;

class SiteHeader extends ViewModel
{
    public function __construct()
    {
        parent::__construct('components.site-header');
    }

    public function getHref(): string
    {
        return URL::route(HomeController::class);
    }

    public function getBrand(): string
    {
        return Config::get('app.name');
    }

    public function getSpreadsheetHref(): string
    {
        return URL::route(SpreadsheetController::class);
    }
}
