<?php

namespace PickSuite\App\Views\Pages;

use PickSuite\App\Views\ViewModel;
use function array_filter;

class GenericPage extends ViewModel
{
    private $args;

    public function __construct(...$args)
    {
        parent::__construct('pages.debug');
        $this->args = $args;
    }

    public function getArgs(): array
    {
        return array_filter($this->args, function ($arg) {
            return !($arg instanceof ViewModel);
        });
    }

    public function getVMs(): array
    {
        return array_filter($this->args, function ($arg) {
            return $arg instanceof ViewModel;
        });
    }
}
