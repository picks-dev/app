<?php

namespace PickSuite\App\Views\Pages;

use PickSuite\App\Views\Components\Table;
use PickSuite\App\Views\Forms\SuggestForm;
use PickSuite\App\Views\ViewModel;

class SuggestionPage extends ViewModel
{
    private $form;
    private $tables;

    public function __construct(SuggestForm $form, iterable $tables)
    {
        parent::__construct('pages.suggestion');
        $this->form = $form;
        $this->tables = $tables;
    }

    public function getForm(): SuggestForm
    {
        return $this->form;
    }

    /**
     * @return iterable|Table[]
     */
    public function getTables(): iterable
    {
        return $this->tables;
    }
}
