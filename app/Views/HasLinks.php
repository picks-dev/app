<?php

namespace PickSuite\App\Views;

trait HasLinks
{
    private $links = [];

    public function getLinks(): array
    {
        return $this->links;
    }

    private function processLinks(iterable $links)
    {
        foreach ($links as $label => $link) {
            [$href, $attributes] = is_array($link) ? $link : [$link, []];
            $this->links[$label] = [$href, $this->attributesToString($attributes)];
        }
    }

    private function attributesToString(array $attributes): string
    {
        $string = '';
        foreach ($attributes as $attr => $value) {
            $string .= " {$attr}=\"{$value}\"";
        }
        return $string;
    }
}
