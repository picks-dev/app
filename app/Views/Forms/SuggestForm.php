<?php

namespace PickSuite\App\Views\Forms;

use Illuminate\Support\Collection;
use PickSuite\App\Http\Requests\SuggestionRequest;
use PickSuite\App\Models\Sport;
use PickSuite\App\Models\Team;
use PickSuite\App\Views\ViewModel;
use function uniqid;

/**
 * @mixin SuggestionRequest
 */
class SuggestForm extends ViewModel
{
    public $id;
    /** @var Sport[] $sportOptions */
    public $sportOptions;
    /** @var Team[] $teamOptions */
    public $teamOptions;

    public function __construct(SuggestionRequest $request)
    {
        parent::__construct('forms.suggest-form');
        parent::pushJs(
            'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js'
        );
        parent::pushCss(
            'https://cdn.jsdelivr.net/npm/bootstrap-chosen@1.4.2/bootstrap-chosen.min.css'
        );
        $this->id = uniqid('form');
        $this->request = $request;
        $this->sportOptions = [];
        $this->teamOptions = [];
    }

    public function setSportOptions(Collection $sports): SuggestForm
    {
        $this->sportOptions = $sports->keyBy(Sport::ATTR_UUID);
        return $this;
    }

    public function setTeamsOptions(Collection $teams): SuggestForm
    {
        $this->teamOptions = $teams->keyBy(Team::ATTR_UUID);
        return $this;
    }

    public function __get($name)
    {
        return $this->request->{$name};
    }

    public function __call($name, $arguments)
    {
        return $this->request->{$name}(...$arguments);
    }

    public function sportIsSelected(string $sport): string
    {
        return $sport === $this->sport ? 'selected="selected"' : '';
    }

    public function sidesIsChecked(): string
    {
        return $this->sides ? 'checked="checked"' : '';
    }

    public function teamIsSelected(string $uuid): string
    {
        return $this->teams->contains(function (Team $team) use ($uuid) {
            return $team->uuid === $uuid;
        }) ? 'selected="selected"' : '';
    }
}
