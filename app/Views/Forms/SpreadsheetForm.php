<?php

namespace PickSuite\App\Views\Forms;

use PickSuite\App\Http\Requests\SpreadsheetRequest;
use PickSuite\App\Views\ViewModel;

/**
 * @mixin SpreadsheetRequest
 */
class SpreadsheetForm extends ViewModel
{
    private $request;
    public $sportOptions;

    public function __construct(SpreadsheetRequest $request)
    {
        parent::__construct('forms.spreadsheet-form');
        $this->request = $request;
        $this->sportOptions = [];
    }

    public function __get($name)
    {
        return $this->request->{$name};
    }

    public function __call($name, $arguments)
    {
        return $this->request->{$name}(...$arguments);
    }

    public function sportIsSelected(string $sport): string
    {
        return $sport === $this->sport ? 'selected="selected"' : '';
    }

    public function setSportOptions(iterable $sports): SpreadsheetForm
    {
        $this->sportOptions = $sports;
        return $this;
    }
}
