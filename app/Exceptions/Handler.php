<?php

namespace PickSuite\App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Log;
use Mail;
use PickSuite\App\Mail\ExceptionOccured;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use function in_array;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        $inDevelopment = in_array(config('app.env'), ['testing', 'local']);
        if ($this->shouldReport($exception) && !$inDevelopment) {
            $this->sendEmail($exception); // sends an email
        }
        parent::report($exception);
    }

    private function sendEmail(Exception $exception)
    {
        if ($email = config('mail.exception')) {
            try {
                $e = FlattenException::create($exception);
                $handler = new SymfonyExceptionHandler();
                $html = $handler->getHtml($e);

                Mail::to($email)->send(new ExceptionOccured($html));
            } catch (Exception $ex) {
                Log::error($ex->getMessage(), $ex->getTrace());
            }
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    protected function invalid($request, ValidationException $exception)
    {
        return $this->invalidJson($request, $exception);
    }
}
