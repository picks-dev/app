<?php

namespace PickSuite\App\Providers;

use DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use PickSuite\App\Models\Model;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        Carbon::setToStringFormat('Y-m-d');
        $this->extendBlueprint();
    }

    private function extendBlueprint()
    {
        Blueprint::mixin(new class
        {
            public function init()
            {
                return function () {
                    $this->engine = 'InnoDB';
                    $this->charset = 'utf8';
                    $this->collation = 'utf8_unicode_ci';
                    $this->increments(Model::ATTR_ID);
                    $this->uuid(Model::ATTR_UUID)->unique();
                    $this->timestamp(Model::ATTR_CREATED_AT)->default(DB::raw('CURRENT_TIMESTAMP'));
                    $this->timestamp(Model::ATTR_UPDATED_AT)->default(DB::raw(
                        DB::getDriverName() === 'sqlite'
                            ? 'CURRENT_TIMESTAMP'
                            : 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
                    ));
                };
            }

            public function relate()
            {
                return function (string $thisForeignKey, string $model): array {
                    /** @var string|Model $model */
                    $field = $this->unsignedInteger($thisForeignKey);
                    $relation = $this
                        ->foreign($thisForeignKey)
                        ->references($model::ATTR_ID)
                        ->on($model::TABLE);

                    $this->index($thisForeignKey);

                    return [$field, $relation];
                };
            }
        });
    }
}
