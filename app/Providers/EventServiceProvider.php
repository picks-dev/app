<?php

namespace PickSuite\App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use PickSuite\App\Listeners\ScrapedGamesListener;
use PickSuite\App\Listeners\ScrapedPicksListener;
use PickSuite\App\Listeners\ScrapedTeamsListener;
use PickSuite\WebScraper\Events\GamesScraped;
use PickSuite\WebScraper\Events\PicksScraped;
use PickSuite\WebScraper\Events\TeamsScraped;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        TeamsScraped::class => [
            ScrapedTeamsListener::class,
        ],
        GamesScraped::class => [
            ScrapedGamesListener::class,
        ],
        PicksScraped::class => [
            ScrapedPicksListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
