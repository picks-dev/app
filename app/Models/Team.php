<?php

namespace PickSuite\App\Models;

use Illuminate\Support\Collection;
use PickSuite\App\Builders\TeamBuilder;
use PickSuite\App\Models\Traits\CoversId;
use PickSuite\App\Models\Traits\ScrapedAt;

/**
 * @mixin TeamBuilder
 * @method static TeamBuilder query()
 * @property string $name
 * @property string $abbr
 * @property Sport $sport
 */
class Team extends Model
{
    use CoversId, ScrapedAt;

    const TABLE = 'teams';

    const ATTR_ABBR = 'abbr';
    const ATTR_COVERS_ID = 'covers_id';
    const ATTR_SPORT_ID = 'sport_id';

    const RELATION_SPORT = 'sport';

    protected static $builder = TeamBuilder::class;

    /**
     * @param string $sportId
     * @param string ...$teamIds
     * @return Collection|Team[]
     */
    public static function bySport(string $sportId, string ...$teamIds): Collection
    {
        $query = Team::query()->whereSport($sportId);
        $teams = ($teamIds ? $query->whereId(...$teamIds) : $query)->get();
        return $teams->isEmpty() ? $query->get() : $teams;
    }

    public function sport()
    {
        return $this->belongsTo(
            Sport::class,
            static::ATTR_SPORT_ID,
            Sport::ATTR_ID,
            static::RELATION_SPORT
        );
    }
}
