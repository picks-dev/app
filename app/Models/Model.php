<?php

namespace PickSuite\App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use PickSuite\App\Builders\Builder;
use PickSuite\Data\Impl\Models\NullModel;
use ReflectionClass;
use ReflectionException;

/**
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static Builder query()
 * @property string id
 * @property string uuid
 * @property Carbon created_at
 * @property Carbon updated_at
 * @method static static make($attributes = [])
 */
abstract class Model extends \Illuminate\Database\Eloquent\Model
{
    const TABLE = null;

    const ATTR_ID = 'id';
    const ATTR_UUID = 'uuid';
    const ATTR_CREATED_AT = self::CREATED_AT;
    const ATTR_UPDATED_AT = self::UPDATED_AT;

    protected static $builder = Builder::class;

    private $cache = [];

    public function __construct(array $attributes = [])
    {
        $this->casts += [
            self::ATTR_CREATED_AT => 'datetime',
            self::ATTR_UPDATED_AT => 'datetime',
        ];
        $this->observables = ['hydrated'];
        $this
            ->setTable(static::TABLE)
            ->fillable(static
                ::enum('ATTR_')
                ->diff(self::enum())
                ->toArray()
            );
        parent::__construct($attributes);
    }

    public static function enum(?string $prefix = 'ENUM_'): Collection
    {
        static $values = [];

        try {
            return $values[$prefix] ?? $values[$prefix] = Collection
                    ::make((new ReflectionClass(static::class))->getConstants())
                    ->filter(function ($_value, string $key) use ($prefix) {
                        return $prefix === null || strpos($key, $prefix) === 0;
                    });
        } catch (ReflectionException $e) {
            return $values[$prefix] = Collection::make();
        }
    }

    public function newEloquentBuilder($query)
    {
        return new static::$builder($query);
    }

    public function getAttribute($key)
    {
        return $this->cache[$key] ?? ($this->cache[$key] = parent::getAttribute($key));
    }

    /**
     * @param array $options
     * @return Model|$this|bool
     */
    public function save(array $options = [])
    {
        $this->setAttribute(static::ATTR_UUID, $this->uuid);
        return parent::save($options) ? $this : false;
    }

    public function setAttribute($key, $value)
    {
        unset($this->cache[$key]);
        return parent::setAttribute($key, $value);
    }

    public function setRelations(array $relations)
    {
        foreach ($relations as $relation => $value) {
            $this->setRelation($relation, $value);
        }
        return $this;
    }

    public function toArray()
    {
        return $this->attributesToArray();
    }

    abstract protected function getUuidAttribute(): string;
}
