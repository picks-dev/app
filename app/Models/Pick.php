<?php

namespace PickSuite\App\Models;

use function ceil;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use PickSuite\App\Builders\Builder;
use PickSuite\App\Models\Traits\ScrapedAt;

/**
 * @property int $rank
 * @property bool $total_over
 * @property string $total_point
 * @property Team|null $side
 * @property Game $game
 * @property Expert $expert
 * @property Team $team
 *
 * @property bool $isTotal
 * @property string $summary
 * @property int $margin
 * @property bool|null $isCorrect
 * @property int $weight
 */
class Pick extends Model
{
    use ScrapedAt;

    const TABLE = 'picks';

    const ATTR_RANK = 'rank';
    const ATTR_TOTAL_OVER = 'total_over';
    const ATTR_TOTAL_POINT = 'total_point';
    const ATTR_SIDE_ID = 'side_id';
    const ATTR_GAME_ID = 'game_id';
    const ATTR_EXPERT_ID = 'expert_id';
    const ATTR_TEAM_ID = 'team_id';

    const RELATION_SIDE = 'side';
    const RELATION_GAME = 'game';
    const RELATION_EXPERT = 'expert';
    const RELATION_TEAM = 'team';

    public $invertedFrom;

    public $casts = [
        self::ATTR_TOTAL_OVER => 'bool',
    ];

    public static function fetchPicks(array $teams, Carbon $from, Carbon $to, string $sportId): array
    {
        return Game
            ::query()
            ->with([
                Game::RELATION_SPORT,
                Game::RELATION_HOME,
                Game::RELATION_AWAY,
                Game::RELATION_SCORES,
                Game::RELATION_PICKS => function (HasMany $hasMany) use ($teams) {
                    return $hasMany->whereIn(Pick::ATTR_TEAM_ID, $teams);
                },
                Game::RELATION_PICKS . '.' . Pick::RELATION_TEAM,
                Game::RELATION_PICKS . '.' . Pick::RELATION_SIDE,
                Game::RELATION_PICKS . '.' . Pick::RELATION_EXPERT,
            ])
            ->whereSport($sportId)
            ->whereDate(Game::ATTR_STARTS_AT, '>=', $from)
            ->whereDate(Game::ATTR_STARTS_AT, '<=', $to)
            ->orderBy(Game::ATTR_STARTS_AT)
            ->where(function (Builder $builder) use ($teams) {
                return $builder
                    ->whereIn(Game::ATTR_AWAY_TEAM_ID, $teams)
                    ->orWhereIn(Game::ATTR_HOME_TEAM_ID, $teams);
            })
            ->get()
            ->reduce(function (array $acc, Game $game) {
                foreach ($game->picks as $pick) {
                    $pick->setRelation(Pick::RELATION_GAME, $game);
                    $team = $pick->team->id;
                    $side = $pick->total_over === null ? 's' : 't';
                    $date = (string)$game->starts_at;
                    $summ = $pick->summary;
                    $acc[$team][$side][$date][$summ] = $pick;
                }
                return $acc;
            }, []);
    }

    public function side(): BelongsTo
    {
        return $this->belongsTo(
            Team::class,
            static::ATTR_SIDE_ID,
            Team::ATTR_ID,
            static::RELATION_SIDE
        );
    }

    public function game(): BelongsTo
    {
        return $this->belongsTo(
            Game::class,
            static::ATTR_GAME_ID,
            Game::ATTR_ID,
            static::RELATION_GAME
        );
    }

    public function expert(): BelongsTo
    {
        return $this->belongsTo(
            Expert::class,
            static::ATTR_EXPERT_ID,
            Expert::ATTR_ID,
            static::RELATION_GAME
        );
    }

    public function team(): BelongsTo
    {
        return $this->belongsTo(
            Team::class,
            static::ATTR_TEAM_ID,
            Team::ATTR_ID,
            static::RELATION_TEAM
        );
    }

    public function invert(): Pick
    {
        if (isset($this->invertedFrom)) {
            return $this->invertedFrom;
        }

        $game = $this->game;
        $away = $game->away;
        $home = $game->home;
        $point = $this->total_point;
        $over = $this->total_over;
        $side = $this->side;

        if ($side !== null) {
            $side = $side->is($away) ? $home : $away;
        }

        if ($point !== null) {
            $point += $over ? 0.5 : -0.5;
            $over = !$over;
        }

        $team = $this->team;
        $expert = $this->expert;

        $invert = static
            ::make([
                static::ATTR_TOTAL_POINT => $point,
                static::ATTR_TOTAL_OVER => $over,
                static::ATTR_SIDE_ID => $side->id ?? null,
                static::ATTR_GAME_ID => $game->id,
                static::ATTR_TEAM_ID => $team->id,
                static::ATTR_EXPERT_ID => $expert->id,
                static::ATTR_RANK => $this->rank,
            ])
            ->setRelations([
                static::RELATION_SIDE => $side,
                static::RELATION_GAME => $game,
                static::RELATION_EXPERT => $expert,
                static::RELATION_TEAM => $team,
            ]);

        $invert->invertedFrom = $this;

        return $invert;

    }

    public function __toString()
    {
        return $this->summary;
    }

    public function getWeightAttribute(): int
    {
        return $this->game->picks->reduce(function (int $weight, Pick $pick) {
            return $weight + (int)($pick->summary === $this->summary);
        }, 0);
    }

    protected function getUuidAttribute(): string
    {
        return implode('_', array_map(function (string $part) {
            return substr($part, 0, 6);
        }, [
            $this->game->uuid,
            $this->expert->uuid,
            $this->team->uuid,
            $this->total_point === null ? 'side' : 'total',
            $this->total_point === null
                ? ($this->side->abbr)
                : ($this->total_over ? '>' : '<') . $this->total_point,
        ]));
    }

    protected function getIsTotalAttribute(): bool
    {
        return $this->total_point !== null;
    }

    protected function getSummaryAttribute(): string
    {
        $gameSummary = $this->game->summary;
        $pickSummary = $this->isTotal
            ? ($this->total_over ? 'ov' : 'un') . $this->total_point
            : $this->side->abbr;
        return "{$gameSummary} {$pickSummary}";
    }

    public function getMarginAttribute(): int
    {
        return $this->game->picks->reduce(function (int $margin, Pick $pick) {
            $agrees = $pick->agreesWith($this);
            return $agrees === null ? $margin : $margin + ($agrees ? 1 : -1);
        }, 0);
    }

    public function agreesWith(Pick $target): ?bool
    {
        if ($this->isTotal !== $target->isTotal) {
            return null;
        }
        if (!$this->game->is($target->game)) {
            return null;
        }
        if (!$this->team->is($target->team)) {
            return null;
        }
        if ($this->isTotal) {
            if ($this->total_over && $target->total_over) {
                return $this->total_point >= $target->total_point;
            }
            if (!$this->total_over && !$target->total_over) {
                return $this->total_point <= $target->total_point;
            }
            return null;
        }
        return $this->side->is($target->side);
    }

    protected function getIsCorrectAttribute(): ?bool
    {
        if (!$this->game->isFinished) {
            return null;
        }

        if ($this->isTotal) {
            return $this->total_over
                ? $this->total_point < $this->game->total
                : $this->total_point > $this->game->total;
        }

        return $this->side->is($this->game->winner);
    }
}
