<?php

namespace PickSuite\App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property string $index
 * @property int $away_score
 * @property int $home_score
 * @property Game $game
 */
class Score extends Model
{
    const TABLE = 'scores';

    const ATTR_INDEX = 'index';
    const ATTR_GAME_ID = 'game_id';
    const ATTR_AWAY_SCORE = 'away_score';
    const ATTR_HOME_SCORE = 'home_score';

    const RELATION_GAME = 'game';

    public function game(): BelongsTo
    {
        return $this->belongsTo(
            Game::class,
            static::ATTR_GAME_ID,
            Game::ATTR_ID,
            static::RELATION_GAME
        );
    }

    protected function getUuidAttribute(): string
    {
        return "{$this->game->uuid}_{$this->index}";
    }
}
