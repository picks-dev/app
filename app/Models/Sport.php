<?php

namespace PickSuite\App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use PickSuite\App\Helpers\Arr;

/**
 * @property string $name
 * @property string $abbr
 * @property bool $is_money_line
 * @property Collection|Team[] $teams
 * @property Carbon $from
 */
class Sport extends Model
{
    const SPORTS = [
        'mlb' => ['baseball', true, '2018-03-29'],
        'nhl' => ['hockey', true, '2018-08-03'],
        'nba' => ['basketball', false, '2018-08-16'],
        'nfl' => ['football', false, '2018-07-06'],
        'wnba' => ['womens basketball', false, null],
        'ncaab' => ['college basketball', false, null],
    ];

    const TABLE = 'sports';

    const ATTR_ABBR = 'abbr';
    const ATTR_NAME = 'name';
    const ATTR_IS_MONEY_LINE = 'is_money_line';
    const ATTR_FROM = 'from';

    const RELATION_TEAMS = 'teams';

    public $casts = [
        self::ATTR_FROM => 'date',
    ];

    public static function listInMemory(): array
    {
        return Arr::map(static::SPORTS, function (array $props, string $uuid) {
            [$name, $isMoneyLine, $from] = $props;
            return static::make([
                static::ATTR_UUID => $uuid,
                static::ATTR_ABBR => $uuid,
                static::ATTR_NAME => $name,
                static::ATTR_IS_MONEY_LINE => $isMoneyLine,
                static::ATTR_FROM => $from ? Carbon::parse($from) : null,
            ]);
        });
    }

    public function teams(): HasMany
    {
        return $this->hasMany(Team::class, Team::ATTR_SPORT_ID, static::ATTR_ID);
    }

    protected function getUuidAttribute(): string
    {
        return $this->abbr;
    }
}
