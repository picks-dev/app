<?php

namespace PickSuite\App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use PickSuite\App\Builders\SimulationBuilder;
use PickSuite\App\Simulation\SimulationResults;
use PickSuite\App\Simulation\SimulationSettings;
use function is_string;
use function uniqid;
use function unserialize;

/**
 * @mixin SimulationBuilder
 * @method static SimulationBuilder query()
 * @property SimulationSettings $settings
 * @property SimulationResults $results
 * @property float $confidence
 * @property ?float $accuracy
 * @property bool $sides
 * @property Carbon $target_date
 * @property Team $team
 */
class Simulation extends Model
{
    const TABLE = 'simulations';

    const ATTR_SETTINGS = 'settings';
    const ATTR_RESULTS = 'results';
    const ATTR_CONFIDENCE = 'confidence';
    const ATTR_ACCURACY = 'accuracy';
    const ATTR_SIDES = 'sides';
    const ATTR_TARGET_DATE = 'target_date';
    const ATTR_TEAM_ID = 'team_id';
    const RELATION_TEAM = 'team';

    protected static $builder = SimulationBuilder::class;

    public $casts = [
        self::ATTR_SETTINGS => SimulationSettings::class,
        self::ATTR_RESULTS => SimulationResults::class,
        self::ATTR_SIDES => 'bool',
        self::ATTR_TARGET_DATE => 'date',
    ];

    public function team(): BelongsTo
    {
        return $this->belongsTo(
            Team::class,
            static::ATTR_TEAM_ID,
            Team::ATTR_ID,
            static::RELATION_TEAM
        );
    }

    public function isBetterThan(Simulation $other): bool
    {
        // Better confidence
        if ($this->confidence !== null && $this->confidence >= $other->confidence) {
            return true;
        }
        // Less dissonance
        if ($this->confidence === $other->confidence && abs($this->results->dissonance) <= abs($other->results->dissonance)) {
            return true;
        }

        return false;
    }

    protected function setResultsAttribute(SimulationResults $results)
    {
        $this->attributes = [
                static::ATTR_RESULTS => $results,
                static::ATTR_CONFIDENCE => $results->confidence,
                static::ATTR_ACCURACY => $results->accuracy,
            ] + $this->attributes;
    }

    protected function setSettingsAttribute(SimulationSettings $settings)
    {
        $this->attributes = [
                static::ATTR_SETTINGS => $settings,
                static::ATTR_SIDES => $settings->sides,
                static::ATTR_TARGET_DATE => $settings->targetDate,
                static::ATTR_TEAM_ID => $settings->team,
            ] + $this->attributes;
    }

    protected function getResultsAttribute($results): SimulationResults
    {
        if ($results instanceof SimulationResults) {
            return $results;
        }
        if (is_string($results)) {
            return unserialize($results);
        }
        return null;
    }

    protected function getSettingsAttribute($settings): ?SimulationSettings
    {
        if ($settings instanceof SimulationSettings) {
            return $settings;
        }
        if (is_string($settings)) {
            return unserialize($settings);
        }
        return null;
    }

    protected function getUuidAttribute(): string
    {
        return $this->attributes[static::ATTR_UUID]
            ?? $this->attributes[static::ATTR_UUID] = uniqid(static::TABLE);
    }
}
