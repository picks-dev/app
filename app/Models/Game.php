<?php

namespace PickSuite\App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use PickSuite\App\Builders\GameBuilder;
use PickSuite\App\Models\Traits\CoversId;
use PickSuite\App\Models\Traits\ScrapedAt;
use function http_build_query;
use function sprintf;
use function strpos;

/**
 * @mixin GameBuilder
 * @method static GameBuilder query()
 * @property string $status
 * @property Carbon $starts_at
 * @property float $away_money_line
 * @property float $home_money_line
 * @property float $over_under
 * @property Sport $sport
 * @property Team $away
 * @property Team $home
 * @property Collection|Score[] $scores
 * @property Collection|Pick[] $picks
 *
 * @property string $summary
 * @property string $consensus_href
 * @property bool $isFinished
 * @property int $total
 * @property Team|null winner
 * @property float|null $computedOu
 */
class Game extends Model
{
    use CoversId, ScrapedAt;

    const TABLE = 'games';

    const ATTR_COVERS_ID = 'covers_id';
    const ATTR_STATUS = 'status';
    const ATTR_STARTS_AT = 'starts_at';
    const ATTR_SPORT_ID = 'sport_id';
    const ATTR_AWAY_TEAM_ID = 'away_id';
    const ATTR_HOME_TEAM_ID = 'home_id';
    const ATTR_AWAY_MONEY_LINE = 'away_money_line';
    const ATTR_HOME_MONEY_LINE = 'home_money_line';
    const ATTR_OVER_UNDER = 'over_under';

    const RELATION_SPORT = 'sport';
    const RELATION_AWAY = 'away';
    const RELATION_HOME = 'home';
    const RELATION_SCORES = 'scores';
    const RELATION_PICKS = 'picks';

    public static $ADJUST_ODDS = false;
    public static $PERIOD_LIMIT = null;

    protected static $builder = GameBuilder::class;

    protected $casts = [
        self::ATTR_STARTS_AT => 'datetime',
        self::ATTR_AWAY_MONEY_LINE => 'float',
        self::ATTR_HOME_MONEY_LINE => 'float',
        self::ATTR_OVER_UNDER => 'float',
    ];

    public function sport(): BelongsTo
    {
        return $this->belongsTo(
            Sport::class,
            static::ATTR_SPORT_ID,
            Sport::ATTR_ID,
            static::RELATION_SPORT
        );
    }

    public function away(): BelongsTo
    {
        return $this->belongsTo(
            Team::class,
            static::ATTR_AWAY_TEAM_ID,
            Team::ATTR_ID,
            static::RELATION_AWAY
        );
    }

    public function home(): BelongsTo
    {
        return $this->belongsTo(
            Team::class,
            static::ATTR_HOME_TEAM_ID,
            Team::ATTR_ID,
            static::RELATION_HOME
        );
    }

    public function scores(): HasMany
    {
        return $this
            ->hasMany(Score::class, Score::ATTR_GAME_ID, static::ATTR_ID)
            ->orderBy(Score::ATTR_INDEX);
    }

    public function picks(): HasMany
    {
        return $this->hasMany(Pick::class, Pick::ATTR_GAME_ID, static::ATTR_ID);
    }

    protected function getSummaryAttribute(): string
    {
        return sprintf(
            '%s vs %s @ %s',
            $this->away->abbr,
            $this->home->abbr,
            $this->starts_at->format('Y-m-d H:i')
        );
    }

    protected function getConsensusHrefAttribute(): string
    {
        $query = ['externalId' => sprintf('/sport/%s/competition:%s', $this->sport->name, $this->uuid)];
        return 'https://contests.covers.com/Consensus/MatchupConsensusDetails?' . http_build_query($query);
    }

    protected function getIsFinishedAttribute(): bool
    {
        return strpos($this->status, 'final') === 0
            || $this->starts_at->diffInHours() >= 12;
    }

    protected function getTotalAttribute(): int
    {
        return $this->scores->reduce(function (int $total, Score $score) {
            return $total + $score->away_score + $score->home_score;
        }, 0);
    }

    protected function getWinnerAttribute(): ?Team
    {
        [$awayScore, $homeScore] = $this->scores->reduce(function (array $scores, Score $score) {
            return [
                $scores[0] + $score->away_score,
                $scores[1] + $score->home_score,
            ];
        }, [0, 0]);

        if ($awayScore > $homeScore) {
            return $this->away;
        } elseif ($awayScore < $homeScore) {
            return $this->home;
        }
        return null;
    }

    protected function getComputedOuAttribute(): ?float
    {
        return $this->isFinished && $this->winner !== null
            ? $this->total - $this->over_under
            : null;
    }
}
