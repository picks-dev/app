<?php

namespace PickSuite\App\Models;

use PickSuite\App\Models\Traits\CoversId;
use PickSuite\App\Models\Traits\ScrapedAt;

class Expert extends Model
{
    use CoversId, ScrapedAt;

    const TABLE = 'experts';

    const ATTR_COVERS_ID = 'covers_id';
}
