<?php

namespace PickSuite\App\Models\Traits;

use Illuminate\Support\Carbon;

/**
 * @property Carbon $scraped_at
 */
trait ScrapedAt
{
    public function updateScrapedAt()
    {
        $this->updated_at = Carbon::now();
        return $this;
    }

    protected function getScrapedAtAttribute(): Carbon
    {
        return $this->updated_at;
    }
}
