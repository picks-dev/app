<?php

namespace PickSuite\App\Models\Traits;

/**
 * @property string $covers_id
 */
trait CoversId
{
    protected function getUuidAttribute(): string
    {
        return $this->covers_id;
    }
}
