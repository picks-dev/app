<?php

namespace PickSuite\App;

use Illuminate\Support\Collection;
use PickSuite\App\Models\Game;
use PickSuite\App\Models\Pick;
use PickSuite\App\Models\Score;
use function array_combine;

class SpreadsheetPicks extends Collection
{
    const HEADERS = [
        'date',
        'day',
        'aw team',
        'ho team',
        'wi team',
        'o/u',
        'aw runs',
        'ho runs',
        'start time',
        'away on',
        'away against',
        'away over',
        'away under',
        'home on',
        'home against',
        'home over',
        'home under',
    ];

    public static function fromGame(Game $game)
    {
        $counts = $game
            ->picks
            ->groupBy(function (Pick $pick) use ($game) {
                $foo = $pick->team->is($game->away) ? 'away' : 'home';
                $bar = $pick->isTotal
                    ? ($pick->total_over ? 'over' : 'under')
                    : ($pick->team->is($pick->side) ? 'on' : 'against');

                return $foo . ucfirst($bar);
            })
            ->map(function (Collection $picks) {
                return $picks->count();
            });

        return parent::make(array_combine(static::HEADERS, [
            $game->starts_at->format('ymd'),
            $game->starts_at->format('D'),
            $game->away->abbr,
            $game->home->abbr,
            $game->winner ? $game->winner->abbr : null,
            $game->computedOu,
            $game->scores->sum(function (Score $score) {
                return $score->away_score;
            }),
            $game->scores->sum(function (Score $score) {
                return $score->home_score;
            }),
            $game->starts_at->format('H'),
            $counts['awayOn'] ?? 0,
            $counts['awayAgainst'] ?? 0,
            $counts['awayOver'] ?? 0,
            $counts['awayUnder'] ?? 0,
            $counts['homeOn'] ?? 0,
            $counts['homeAgainst'] ?? 0,
            $counts['homeOver'] ?? 0,
            $counts['homeUnder'] ?? 0,
        ]));
    }
}
