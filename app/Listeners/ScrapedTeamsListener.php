<?php

namespace PickSuite\App\Listeners;

use Illuminate\Support\Collection;
use Log;
use PickSuite\App\Helpers\Meta;
use PickSuite\App\Models\Sport;
use PickSuite\App\Models\Team;
use PickSuite\WebScraper\Covers\TeamPage;
use PickSuite\WebScraper\Events\TeamsScraped;
use function sprintf;

class ScrapedTeamsListener
{
    public function handle(TeamsScraped $event)
    {
        Meta::setTeamsScrapedAt($event->sport);
        $teams = Collection::make($event->teams);
        Log::info(sprintf('scraped %d teams.', $teams->count()));

        $sport = $this->fetchSport($event->sport);
        $this->persistTeams($teams, $sport);
    }

    private function fetchSport(string $id): Sport
    {
        /** @var Sport $sport */
        $sport = Sport
            ::query()
            ->with([Sport::RELATION_TEAMS])
            ->whereId($id)
            ->firstOrFail();

        return $sport;
    }

    private function persistTeams(Collection $teams, Sport $sport): void
    {
        /** @var Collection|Team[] $existingTeams */
        $existingTeams = $sport->teams->keyBy(Team::ATTR_UUID);
        $teams
            ->map(function (TeamPage $teamPage) use ($sport) {
                return Team::make([
                    Team::ATTR_ABBR => $teamPage->abbr,
                    Team::ATTR_COVERS_ID => $teamPage->id,
                    Team::ATTR_SPORT_ID => $sport->id,
                ]);
            })
            ->keyBy(Team::ATTR_UUID)
            ->each(function (Team $team, string $uuid) use ($existingTeams) {
                ($existingTeams[$uuid] ?? $team)
                    ->fill($team->getAttributes())
                    ->updateScrapedAt()
                    ->saveOrFail();
            });
    }
}
