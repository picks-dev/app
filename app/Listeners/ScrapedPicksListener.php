<?php

namespace PickSuite\App\Listeners;

use Bus;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Log;
use PickSuite\App\Helpers\Meta;
use PickSuite\App\Jobs\ScrapePicks;
use PickSuite\App\Models\Expert;
use PickSuite\App\Models\Game;
use PickSuite\App\Models\Pick;
use PickSuite\WebScraper\Covers\PickPartial;
use PickSuite\WebScraper\Events\PicksScraped;
use Queue;

class ScrapedPicksListener
{
    private $picks;

    public function __construct()
    {
        $this->picks = Collection::make();
    }

    public function handle(PicksScraped $event)
    {
        Meta::setPicksScrapedAt($event->sport);
        $picks = Collection::make($event->picks);
        Log::info(sprintf('scraped %d picks.', $picks->count()));

        $experts = $this->mapExperts($picks);
        $existingExperts = $this->fetchExistingExperts($experts->keys());
        $experts = $this->persistExperts($experts, $existingExperts);

        $games = $this->fetchGames($picks);
        $picks = $this->mapPicks($picks, $games, $experts);
        $existingPicks = $this->fetchExistingPicks($picks->keys());
        $this->persistPicks($picks, $existingPicks);

        if (Queue::getDefaultDriver() !== 'sync' && $event->recurring) {
            $splay = rand(45, 75);
            $targetDate = Carbon::now()->addMinutes($splay);
            Bus::dispatch((new ScrapePicks($event->sport, $targetDate))->delay($splay * 60));
        }
    }

    private function mapExperts(Collection $picks): Collection
    {
        return $picks
            ->map(function (PickPartial $pick) {
                return Expert::make([
                    Expert::ATTR_COVERS_ID => $pick->expert_id,
                ]);
            })
            ->keyBy(Expert::ATTR_UUID);
    }

    private function fetchExistingExperts(Collection $ids): Collection
    {
        return Expert
            ::query()
            ->whereId(...$ids->toArray())->get()
            ->keyBy(Expert::ATTR_UUID);
    }

    private function persistExperts(Collection $experts, Collection $existingExperts): Collection
    {
        return $experts->map(function (Expert $expert, string $uuid) use ($existingExperts) {
            return ($existingExperts[$uuid] ?? $expert)
                ->fill($expert->getAttributes())
                ->updateScrapedAt()
                ->saveOrFail();
        });
    }

    private function fetchGames(Collection $picks): Collection
    {
        $gameIds = $picks->map(function (PickPartial $pick) {
            return $pick->game_id;
        });

        return Game
            ::query()
            ->with([
                Game::RELATION_AWAY,
                Game::RELATION_HOME,
            ])
            ->whereId(...$gameIds->toArray())
            ->get()
            ->keyBy(Game::ATTR_UUID);
    }

    /**
     * @param Collection|Pick[] $picks
     * @param Collection|Game[] $games
     * @param Collection|Expert[] $experts
     * @return Collection
     */
    private function mapPicks(Collection $picks, Collection $games, Collection $experts)
    {
        return $picks
            ->map(function (PickPartial $pick) use ($games, $experts) {
                $game = $games[$pick->game_id];
                $awayId = $game->away->id;
                $homeId = $game->home->id;
                $teamId = [true => $awayId, false => $homeId, null => null][$pick->team_is_away];
                $sideId = [true => $awayId, false => $homeId, null => null][$pick->side_is_away];
                return Pick::make([
                    Pick::ATTR_RANK => $pick->rank,
                    Pick::ATTR_TOTAL_OVER => $pick->total_over,
                    Pick::ATTR_TOTAL_POINT => $pick->total_point,
                    Pick::ATTR_SIDE_ID => $sideId,
                    Pick::ATTR_GAME_ID => $game->id,
                    Pick::ATTR_EXPERT_ID => $experts[$pick->expert_id]->id,
                    Pick::ATTR_TEAM_ID => $teamId,
                ]);
            })
            ->keyBy(Pick::ATTR_UUID);
    }

    private function fetchExistingPicks(Collection $uuids): Collection
    {
        return Pick
            ::query()
            ->whereId(...$uuids->toArray())
            ->get()
            ->keyBy(Pick::ATTR_UUID);
    }

    private function persistPicks(Collection $picks, Collection $existingPicks): void
    {
        $picks->each(function (Pick $pick, string $uuid) use ($existingPicks) {
            $pick = ($existingPicks[$uuid] ?? $pick)
                ->fill($pick->getAttributes());

            if ($pick->isDirty()) {
                $this->picks->push($pick);
            }

            return $pick
                ->updateScrapedAt()
                ->saveOrFail();
        });
    }
}
