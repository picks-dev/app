<?php

namespace PickSuite\App\Listeners;

use Bus;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Log;
use PickSuite\App\Helpers\Meta;
use PickSuite\App\Jobs\ScrapeGames;
use PickSuite\App\Models\Game;
use PickSuite\App\Models\Score;
use PickSuite\App\Models\Sport;
use PickSuite\App\Models\Team;
use PickSuite\WebScraper\Covers\GameBoxPartial;
use PickSuite\WebScraper\Events\GamesScraped;
use function array_map;
use Queue;
use function rand;

class ScrapedGamesListener
{
    public function handle(GamesScraped $event)
    {
        Meta::setGamesScrapedAt($event->sport);
        $gameBoxes = Collection::make($event->games)->keyBy(function (GameBoxPartial $gameBox) {
            return $gameBox->id;
        });
        Log::info(sprintf('scraped %d games.', $gameBoxes->count()));

        $sport = $this->fetchSport($event->sport);

        $games = $this->mapGames($gameBoxes, $sport);
        $existingGames = $this->fetchGames($games->keys());
        $existingGames = $this->persistGames($games, $existingGames);
        $scores = $this->mapScores($gameBoxes, $existingGames);
        $this->persistScores($scores);

        if (Queue::getDefaultDriver() !== 'sync' && $event->recurring) {
            $splay = rand(300, 420);
            $targetDate = Carbon::now()->addMinutes($splay);
            Bus::dispatch((new ScrapeGames($event->sport, $targetDate))->delay($splay * 60));
        }
    }

    private function fetchSport(string $id): Sport
    {
        /** @var Sport $sport */
        $sport = Sport
            ::query()
            ->with([Sport::RELATION_TEAMS])
            ->whereId($id)
            ->firstOrFail();

        return $sport;
    }

    private function mapGames(Collection $games, Sport $sport): Collection
    {
        $teams = $sport->teams->keyBy(Team::ATTR_UUID);
        return Collection
            ::make($games)
            ->map(function (GameBoxPartial $gameBox) use ($sport, $teams) {
                return Game::make([
                    Game::ATTR_COVERS_ID => $gameBox->id,
                    Game::ATTR_STATUS => $gameBox->status,
                    Game::ATTR_STARTS_AT => Carbon::instance($gameBox->start_time),
                    Game::ATTR_SPORT_ID => $sport->id,
                    Game::ATTR_AWAY_TEAM_ID => $teams[$gameBox->away_team_id]->id,
                    Game::ATTR_HOME_TEAM_ID => $teams[$gameBox->home_team_id]->id,
                    Game::ATTR_AWAY_MONEY_LINE => $gameBox->away_money_line,
                    Game::ATTR_HOME_MONEY_LINE => $gameBox->home_money_line,
                    Game::ATTR_OVER_UNDER => $gameBox->over_under,
                ]);
            })
            ->keyBy(Game::ATTR_UUID);
    }

    private function fetchGames(Collection $ids): Collection
    {
        return Game
            ::query()
            ->with([Game::RELATION_SCORES])
            ->whereId(...$ids->toArray())
            ->get()
            ->keyBy(Game::ATTR_UUID);
    }

    private function persistGames(Collection $games, Collection $existingGames): Collection
    {
        return $games
            ->map(function (Game $game, string $uuid) use ($existingGames) {
                return ($existingGames[$uuid] ?? $game)
                    ->fill($game->getAttributes())
                    ->updateScrapedAt()
                    ->saveOrFail();
            })
            ->keyBy(Game::ATTR_UUID);
    }

    /**
     * @param Collection $gameBoxes
     * @param Collection|Game[] $existingGames
     * @return Collection
     */
    private function mapScores(Collection $gameBoxes, Collection $existingGames): Collection
    {
        return $gameBoxes
            ->map(function (GameBoxPartial $gameBox) use ($existingGames) {
                return array_map(function (int $awayScore, string $index) use ($gameBox, $existingGames) {
                    $game = $existingGames[$gameBox->id];
                    return Score
                        ::make([
                            Score::ATTR_INDEX => $index,
                            Score::ATTR_GAME_ID => $game->id,
                            Score::ATTR_AWAY_SCORE => $awayScore,
                            Score::ATTR_HOME_SCORE => $gameBox->home_scores[$index],
                        ])
                        ->setRelation(Score::RELATION_GAME, $game);
                }, $gameBox->away_scores, array_keys($gameBox->away_scores));
            })
            ->flatten()
            ->keyBy(Score::ATTR_UUID);
    }

    private function persistScores(Collection $scores): void
    {
        $scores->each(function (Score $score) {
            $existingScores = $score->game->scores->keyBy(Score::ATTR_UUID);
            /** @var Score $score */
            ($existingScores[$score->uuid] ?? $score)
                ->fill($score->getAttributes())
                ->saveOrFail();
        });
    }
}
