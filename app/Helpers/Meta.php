<?php

namespace PickSuite\App\Helpers;

use Cache;
use DB;
use Illuminate\Support\Carbon;
use PickSuite\App\Jobs\ScrapeGames;
use PickSuite\App\Jobs\ScrapePicks;
use PickSuite\App\Jobs\ScrapeTeams;
use function preg_quote;

class Meta
{
    public static function getTeamsScrapeTimes(string $sport): array
    {
        return [
            Cache::get("{$sport}.teams_scraped_at"),
            static::fetchJob($sport, ScrapeTeams::class),
        ];
    }

    private static function fetchJob(string $sport, string $job): ?Carbon
    {
        $job = preg_quote(preg_quote($job));
        $record = DB
            ::table('jobs')
            ->where('payload', 'like', "%{$sport}%")
            ->where('payload', 'like', "%{$job}%")
            ->orderBy('available_at')
            ->first();
        return $record ? Carbon::createFromTimestamp($record->available_at) : null;
    }

    public static function getGamesScrapeTimes(string $sport): array
    {
        return [
            Cache::get("{$sport}.games_scraped_at"),
            static::fetchJob($sport, ScrapeGames::class),
        ];
    }

    public static function getPicksScrapeTimes(string $sport): array
    {
        return [
            Cache::get("{$sport}.picks_scraped_at"),
            static::fetchJob($sport, ScrapePicks::class),
        ];
    }

    public static function setTeamsScrapedAt(string $sport): void
    {
        Cache::forever("{$sport}.teams_scraped_at", Carbon::now());
    }

    public static function setGamesScrapedAt(string $sport): void
    {
        Cache::forever("{$sport}.games_scraped_at", Carbon::now());
    }

    public static function setPicksScrapedAt(string $sport): void
    {
        Cache::forever("{$sport}.picks_scraped_at", Carbon::now());
    }
}
