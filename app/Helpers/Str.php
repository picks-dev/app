<?php

namespace PickSuite\App\Helpers;

use App;
use Exception;
use Illuminate\Foundation\Bootstrap\HandleExceptions;
use function is_array;
use function json_encode;
use function number_format;
use function round;

class Str extends \Illuminate\Support\Str
{
    public static function consumeException($callback)
    {
        try {
            return (string)App::call($callback);
        } catch (Exception $e) {
            $handler = new HandleExceptions;
            $handler->bootstrap(app());
            $handler->handleException($e);
            die();
        }
    }

    public static function print_r(iterable $arr, string $rowGlue = "\n", string $valueGlue = "\t")
    {
        $res = [];
        foreach ($arr as $data) {
            $row = [];
            foreach ($data as $datum) {
                $row[] = $datum;
            }
            $res[] = implode($valueGlue, $row);
        }

        return implode($rowGlue, $res);
    }

    public static function parse($value): string
    {
        return is_array($value) ? json_encode($value) : $value;
    }

    public static function percent(float $confidence): string
    {
        return static::number($confidence * 100 ) . '%';
    }

    public static function number(float $num, int $precision = 2): string
    {
        return number_format(round($num, $precision), $precision);
    }

}
