<?php

namespace PickSuite\App\Helpers;

use function array_search;
use function array_slice;

class Arr extends \Illuminate\Support\Arr
{
    public static function reduce(iterable $source, callable $callback, $initial = null)
    {
        foreach ($source as $key => $value) {
            $initial = $callback($initial, $value, $key, $source);
        }
        return $initial;
    }

    public static function average(iterable $values, bool $filterNull = true): ?float
    {
        $total = 0;
        $count = 0;
        foreach ($values as $value) {
            $total += $value;
            if ($filterNull && $value !== null) {
                $count++;
            }
        }
        return $count === 0 ? null : $total / max(1, $count);
    }

    public static function fifo(array $items, int $size = 1): array
    {
        return Arr::map($items, function ($_item, $key, array $items) use ($size) {
            return Arr::sliceAt($items, $key, $size);
        });
    }

    public static function map(iterable $items, callable $cb): array
    {
        $result = [];
        foreach ($items as $key => $item) {
            $result[$key] = $cb($item, $key, $items);
        }
        return $result;
    }

    public static function sliceAt(array $items, $target, int $size): array
    {
        $index = array_search($target, array_keys($items), true);
        $offset = $size > 0 ? $index : $index + $size + 1;
        $take = abs($size) + min(0, $offset);
        return array_slice($items, max(0, $offset), $take);
    }

    public static function shift(iterable $items): array
    {
        $previous = null;
        return Arr::map($items, function ($item) use (&$previous) {
            $result = $previous;
            $previous = $item;
            return $result;
        });
    }
}
