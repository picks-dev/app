<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use PickSuite\App\Http\Controllers\HomeController;
use PickSuite\App\Http\Controllers\SpreadsheetController;
use PickSuite\App\Http\Controllers\SuggestController;

Route::get('/', HomeController::toRoute());
Route::get('/spreadsheet', SpreadsheetController::toRoute());
Route::get('/suggest', SuggestController::toRoute());
