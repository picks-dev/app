function copyToClipboard(text) {
    let textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();

    try {
        let successful = document.execCommand('copy');
        if (!successful) {
            throw new Error(`Copying text was unsuccessful`);
        }
    } catch (err) {
        console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
}

document.addEventListener("DOMContentLoaded", function (event) {
    Array.prototype.forEach.call(document.querySelectorAll('*[data-copy]'), function (copyable) {
        copyable.addEventListener('click', function (evt) {
            copyToClipboard(evt.target.dataset.copy);
        });
    });
});
